""" Tickets views."""

# Django

# Django Rest Framework
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

# Own
from attis_v2.api.serializers import TicketModelSerializer, TicketCreateSerializer, TicketStatusSerializer
from attis_v2.core.models import Ticket
from attis_v2.taskapp.tasks import new_ticket

class TicketViewSet(mixins.RetrieveModelMixin,
                    mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    viewsets.GenericViewSet):
    """ Tickets viewset
        Handle all CRUD requests.
    """

    def get_serializer_class(self):
        if self.action == 'create':
            return TicketCreateSerializer
        if self.action == 'retrieve':
            return TicketStatusSerializer
        if self.action == 'list':
            return TicketStatusSerializer
        return TicketModelSerializer

    def get_queryset(self):
        if self.action == 'list':
            account_id = self.request.GET.get("account_id", None)
            return Ticket.objects.filter(account_id=account_id)
        return Ticket.objects.all()

    @action(detail=False, methods=['get'])
    def load(self, request):
        import math
        x = 0.0001
        for i in range(0, 1000000):
            x += math.sqrt(x)
        return Response("OK", status=status.HTTP_200_OK)
