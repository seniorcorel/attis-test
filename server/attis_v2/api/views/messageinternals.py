""" Message internal views."""

# Django Rest Framework
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend

# Own
from attis_v2.api.serializers import MessageInternalModelSerializer
from attis_v2.core.models import MessageInternal
from attis_v2.api.permissions import IsLevelSupervisor


class MessageInternalViewSet(viewsets.ModelViewSet):
    """ Message Internal viewset
        Handle all CRUD requests.
    """

    queryset = MessageInternal.objects.filter(is_active=True)
    serializer_class = MessageInternalModelSerializer
    permission_classes = [IsAuthenticated, IsLevelSupervisor]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['account_id']

    def perform_destroy(self, instance):
        """ Implement soft delete."""
        instance.is_active = False
        instance.save()

    def paginate_queryset(self, queryset):
        """ Necessary filter to obtain all the records without page (it is useful to list these
        objects in selects in frontend). """
        if 'no_page' in self.request.query_params:
            return None
        return super().paginate_queryset(queryset)

