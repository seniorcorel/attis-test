""" Autoreject views."""

# Django Rest Framework
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend


# Own
from attis_v2.api.serializers import AutorejectModelSerializer
from attis_v2.core.models import Autoreject
from attis_v2.api.permissions import IsLevelSupervisor


class AutorejectViewSet(viewsets.ModelViewSet):
    """ Autorejects viewset
        Handle all CRUD requests.
    """

    queryset = Autoreject.objects.filter(is_active=True)
    serializer_class = AutorejectModelSerializer
    permission_classes = [IsAuthenticated, IsLevelSupervisor]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['account_id']

    def perform_destroy(self, instance):
        """ Implement soft delete."""
        instance.is_active = False
        instance.save()

