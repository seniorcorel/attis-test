# """ Reports views."""
#
# # Django
# from django.utils import timezone
# from django.db.models import Avg, Count
# # from django.contrib.auth import get_user_model
#
# # Django Rest Framework
# from rest_framework import viewsets, status
# from rest_framework.permissions import IsAuthenticated
# from rest_framework.decorators import action
# from rest_framework.response import Response
#
# # Own
# from attis_v2.api.permissions import IsLevelSupervisor
# from attis_v2.core.models import Ticket, Taken
# from attis_v2.users.models import User
#
# from datetime import datetime, timedelta
# import itertools
#
# class ReportsViewSet(viewsets.ModelViewSet):
#     """ Reports viewset
#         Handle all reports o statistics requests.
#     """
#
#     queryset = Ticket.objects.all()
#     serializer_class = None
#     permission_classes = [IsAuthenticated, IsLevelSupervisor]
#
#     def get_start_date(self, hours):
#         return datetime.now(timezone.utc) - timedelta(hours=hours)
#
#     @action(detail=False, methods=['get'])
#     def line_stats(self, request):
#         """ Get pending and working tickets on last 24h. """
#
#         start = self.get_start_date(24)
#         data = {
#             'pending': Ticket.objects.filter(arrived__gte=start, status=Ticket.PENDING).count(),
#             'working': Ticket.objects.filter(arrived__gte=start, status=Ticket.WORKING).count()
#         }
#         return Response(data, status=status.HTTP_200_OK)
#
#     @action(detail=False, methods=['get'])
#     def times_stats(self, request):
#         """ Get AVG of waittime and duration time on tickets on last 24h. """
#
#         start = self.get_start_date(24)
#         data = {
#             'waittime': Ticket.objects.filter(arrived__gte=start, status__gt=Ticket.WORKING).aggregate(Avg('waittime'))
#                 ['waittime__avg'],
#             'duration': Taken.objects.filter(start__gte=start, status__gt=Ticket.WORKING).aggregate(Avg('duration'))
#                 ['duration__avg'],
#         }
#         return Response(data, status=status.HTTP_200_OK)
#
#     @action(detail=False, methods=['get'])
#     def users_stats(self, request):
#         """ Get quantity of users by status. """
#
#         data = User.objects.filter(is_active=True, is_staff=False, level__lte=User.SUPERVISOR).\
#             values('status').\
#             annotate(total=Count('status')).\
#             order_by('status')
#         return Response(data, status=status.HTTP_200_OK)
#
#     @action(detail=False, methods=['get'])
#     def tickets_stats(self, request):
#         """ Get quantity of tickets by status """
#
#         start = self.get_start_date(24)
#         data = Ticket.objects.filter(arrived__gte=start, status__gt=Ticket.WORKING).\
#             values('status').\
#             annotate(total=Count('status')).\
#             order_by('status')
#         return Response(data, status=status.HTTP_200_OK)
#
#     @action(detail=False, methods=['get'])
#     def volume_by_hours(self, request):
#         """ Get quantity of tickets by hours for a day """
#         date = request.GET.get('date', datetime.today())
#         arr = Ticket.objects.filter(arrived__date=date, status__gte=Ticket.SUCCESS).order_by('arrived')
#         groups = itertools.groupby(arr, lambda x: self.date_hour(x.arrived))
#         result = []
#         for group, matches in groups:
#             result.append({
#                 'hour': group,
#                 'count': sum(1 for _ in matches)
#             })
#         return Response(result, status=status.HTTP_200_OK)
#
#     def date_hour(self, timestamp):
#         return timestamp.strftime("%H")
