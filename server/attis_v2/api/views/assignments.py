""" Assignment views."""
# Django
from django.shortcuts import get_object_or_404

# Django Rest Framework
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

# Own
from attis_v2.api.serializers import Assignment_byUserSerializer, LogAssignmentModelSerializer, GroupModelSerializer
from attis_v2.api.permissions import AssignGroups
from attis_v2.core.models import LogAssignment, User, Group
from attis_v2.users.serializers import UserModelSerializer

from attis_v2.taskapp.tasks import new_ticket

class AssignmentView(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    """ Assignment view
        Handle assign by user and assign by group
    """

    queryset = LogAssignment.objects.all()
    serializer_class = LogAssignmentModelSerializer
    permission_classes = [IsAuthenticated, AssignGroups]

    @action(detail=False, methods=['post'])
    def assign_by_user(self, request):
        """ Assignments of groups by one user. """
        serializer = Assignment_byUserSerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        data = serializer.save()

        return Response(data, status=status.HTTP_200_OK)




    def retrieve(self, request, pk=None):
        """ If OBJECT param is user => return groups assigned.
            If OBJECT param is group => return users assigned in this group. """

        object = self.request.query_params.get('object')

        if object == 'user':
            queryset = User.objects.filter(is_active=True, is_staff=False)
            serializer = GroupModelSerializer
        else:
            queryset = Group.objects.filter(is_active=True)
            serializer = UserModelSerializer
        obj = get_object_or_404(queryset, pk=pk)

        result = obj.my_groups.all() if object == 'user' else obj.users.all()
        return Response(serializer(result, many=True).data)

