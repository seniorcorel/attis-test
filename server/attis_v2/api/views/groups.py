""" Group views."""

# Django Rest Framework
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.http import Http404

# Own
from attis_v2.api.serializers import GroupModelSerializer
from attis_v2.core.models import Group
from attis_v2.api.permissions import IsLevelSupervisor, IsLevelStamdard


class GroupViewSet(viewsets.ModelViewSet):
    """ Groups viewset
        Handle all CRUD requests.
    """

    queryset = Group.objects.filter(is_active=True)
    serializer_class = GroupModelSerializer

    def get_permissions(self):
        if self.request.method == 'GET':
            self.permission_classes = [IsAuthenticated, IsLevelStamdard]
        else:
            self.permission_classes = [IsAuthenticated, IsLevelSupervisor]

        return super(GroupViewSet, self).get_permissions()

    def perform_destroy(self, instance):
        """ Implement soft delete."""
        instance.is_active = False
        instance.save()

    def paginate_queryset(self, queryset):
        """ Necessary filter to obtain all the records without page (it is useful to list these
        objects in selects in frontend). """
        if 'no_page' in self.request.query_params:
            return None
        return super().paginate_queryset(queryset)
