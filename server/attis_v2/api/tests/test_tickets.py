""" Tickets tests. """

# Django
from django.urls import reverse
from django.utils import timezone
from django.db.models import Q
from django.db import connection
from django.core.files.uploadedfile import SimpleUploadedFile

# Django REST Framework
from rest_framework import status

# Our
from attis_v2.core.models import Group, Domain, Ticket, FileTicket, Autoreject, MessagePrefix, Duplicate, MessageInternal
from attis_v2.users.models import User
from attis_v2.api.serializers import TicketModelSerializer
from attis_v2.utils.basetest import BaseAPITestCase

# Util
from PIL import Image
import io
import urllib

class TicketTest(BaseAPITestCase):
    """ Testing Api View and Serializer Model of Tickets Objects. """

    def generate_image_file(self):
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)
        return file

    def setUp(self):
        # Utils entities
        self.domain = Domain.objects.create(slug='com-br')
        self.group = Group.objects.create(name='01', slug='01')


    def test_create__success(self):
        """ Test endpoint create with right parameters. """

        post_data_ok = {
            'account_id': 1,
            'account_name': 'x',
            'domain': self.domain.slug,
            'group': self.group.slug,
            'file': self.generate_image_file()
        }

        url = reverse('api:ticket-list')
        response = self.client.post(url, post_data_ok)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(1, Ticket.objects.filter(id=response.data['id']).count())
        self.assertEqual(1, FileTicket.objects.filter(tickets__id=response.data['id']).count())

    def test_create__error_domain_and_group_not_exist(self):
        """ Test endpoint create with wrong parameters, domain and group. """

        post_data_error_domain = {
            'account_id': 1,
            'account_name': 'x',
            'domain': 'no-exist',
            'group': self.group.slug,
            'file': self.generate_image_file()
        }
        url = reverse('api:ticket-list')
        response = self.client.post(url, post_data_error_domain)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        post_data_error_group = {
            'account_id': 1,
            'account_name': 'x',
            'domain': self.domain.slug,
            'group': 'no-exist',
            'file': self.generate_image_file()
        }
        url = reverse('api:ticket-list')
        response = self.client.post(url, post_data_error_group)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create__error_autoreject(self):
        """ Test endpoint create for client in autoreject list. """

        autoreject_data = {
            'account_id': 1,
            'message': MessagePrefix.objects.create(title='01', body='01')
        }
        Autoreject.objects.create(**autoreject_data)

        post_data_error_autoreject = {
            'account_id': autoreject_data['account_id'],
            'account_name': 'x',
            'domain': self.domain.slug,
            'group': self.group.slug,
            'file': self.generate_image_file()
        }
        url = reverse('api:ticket-list')
        response = self.client.post(url, post_data_error_autoreject)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('autoreject' in str(response.data['file'][0]))

    def test_create__error_duplicate_file(self):
        """ Test endpoint create with duplicate file.
            Is necessary reopen the file to refresh file pointer. if not Django takes file as empty.
            This test need all services up. """

        image_test_uri = './staticfiles/test/image_test.jpg'
        post_data_error_duplicate = {
            'account_id': 1,
            'account_name': 'x',
            'domain': self.domain.slug,
            'group': self.group.slug,
            'file': None
        }
        url = reverse('api:ticket-list')

        with open(image_test_uri, "rb") as f1:
            post_data_error_duplicate['file'] = SimpleUploadedFile("file.jpg", f1.read())
            response = self.client.post(url, post_data_error_duplicate)

            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Resend same file
        with open(image_test_uri, "rb") as f2:
            post_data_error_duplicate['file'] = SimpleUploadedFile("file.jpg", f2.read())
            response = self.client.post(url, post_data_error_duplicate)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertTrue('duplicate' in str(response.data['file'][0]))

    def test_TicketModelSerializer_get_internals__success(self):
        """ Test get_internals function in TicketModelSerializer with result successfully. """

        account_id = 1

        # Create data
        message = MessageInternal.objects.create(account_id=account_id, message='01')
        post_data_ok = {
            'account_id': account_id,
            'account_name': 'x',
            'domain': self.domain.slug,
            'group': self.group.slug,
            'file': self.generate_image_file()
        }
        url = reverse('api:ticket-list')
        response = self.client.post(url, post_data_ok)

        # Create serializer
        ticket = Ticket.objects.get(id=response.data['id'])
        serializer = TicketModelSerializer(ticket).data

        self.assertEqual(1, len(serializer['internals']))
        self.assertEqual(message.message, serializer['internals'][0]['message'])


    def test_list__success_get_just_clinets_ticket(self):
        """ Test endpoint list and return list client's tickets.  """

        for account_id in [111, 222]:
            post_data_ok = {
                'account_id': account_id,
                'account_name': 'x',
                'domain': self.domain.slug,
                'group': self.group.slug,
                'file': self.generate_image_file()
            }

            url = reverse('api:ticket-list')
            self.client.post(url, post_data_ok)

        # Just one client
        url = reverse('api:ticket-list')
        response = self.client.get(url + '?account_id={}'.format(111))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, len(response.data['results']))

        # No Account id, No ticket
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(0, len(response.data['results']))
