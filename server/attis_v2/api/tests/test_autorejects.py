""" Autorejects tests. """

# Django
from django.urls import reverse
from django.contrib.auth import get_user_model

# Django REST Framework
from rest_framework import status

# Our
from attis_v2.utils.basetest import BaseAPITestCase
from attis_v2.core.models import Autoreject, MessagePrefix



class AutorejectTest(BaseAPITestCase):
    """ Testing Api View and Serializer Model of Autoreject Objects. """

    def test_soft_delete__success(self):
        """ Test custom soft delete with success (override perform_destroy method). """
        autoreject_data = {
            'account_id': 1,
            'message': MessagePrefix.objects.create(title='01', body='01')
        }
        autoreject = Autoreject.objects.create(**autoreject_data)
        self.assertTrue(autoreject.is_active)

        self.login_user({'username': '01', 'password': '123456.a', 'level': get_user_model().SUPERVISOR})
        url = reverse('api:autoreject-detail', args=[autoreject.id])
        response = self.client.delete(url, autoreject_data)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Autoreject.objects.get(id=autoreject.id).is_active)
        self.assertEqual(1, Autoreject.objects.filter(id=autoreject.id).count())
