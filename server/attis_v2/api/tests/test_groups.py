""" Groups tests. """

# Django
from django.urls import reverse
from django.contrib.auth import get_user_model

# Django REST Framework
from rest_framework import status

# Our
from attis_v2.utils.basetest import BaseAPITestCase
from attis_v2.core.models import Group



class GroupTest(BaseAPITestCase):
    """ Testing Api View and Serializer Model of Group Objects. """

    def test_soft_delete__success(self):
        """ Test custom soft delete with success (override perform_destroy method). """

        group = Group.objects.create(name='01', slug='01')
        self.assertTrue(group.is_active)

        self.login_user({'username': '01', 'password': '123456.a', 'level': get_user_model().SUPERVISOR})
        response = self.client.delete(reverse('api:group-detail', args=[group.id]))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Group.objects.get(id=group.id).is_active)
        self.assertEqual(1, Group.objects.filter(id=group.id).count())

    def test_soft_delete__error_no_allowed(self):
        """ Test custom soft delete with no allowed user (override perform_destroy method). """

        group = Group.objects.create(name='01', slug='01')

        self.login_user({'username': '01', 'password': '123456.a', 'level': get_user_model().STANDARD})
        response = self.client.delete(reverse('api:group-detail', args=[group.id]))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrive_no_paginated__success(self):
        """ Test endpoint retrieve with and without no_page parameter, return success. """

        item_by_page = 10
        total_amount = 30
        for i in range(total_amount):
            Group.objects.create(name=str(i), slug=str(i))

        self.login_user({'username': '01', 'password': '123456.a', 'level': get_user_model().STANDARD})

        response_paginated = self.client.get(reverse('api:group-list'))
        self.assertEqual(item_by_page, len(response_paginated.data['results']))

        response_no_paginated = self.client.get(reverse('api:group-list') + '?no_page')
        self.assertEqual(total_amount, len(response_no_paginated.data))


