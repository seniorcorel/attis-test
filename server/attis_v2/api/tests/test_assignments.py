""" Assignments tests. """

# Django
from unittest.mock import MagicMock

# Django REST Framework
from rest_framework import status
from rest_framework.test import APITestCase

# Our
from attis_v2.core.models import Group, LogAssignment
from attis_v2.users.models import User
from attis_v2.api.serializers import Assignment_byUserSerializer

class AssignmentTest(APITestCase):
    """ Testing Api View and Serializer Model of Assignmanet Objetc. """

    def login_user(self, user_data, create=True):
        """ Create and login user to consume API. """

        if create:
            User.objects.create_user(**user_data)

        login_data = {
            'username': user_data['username'],
            'password':  user_data['password']
        }
        response_user = self.client.post('/api/users/login/', login_data)
        self.user_logged = User.objects.get(id=response_user.data['user']['id'])
        self.token = 'Token {}'.format(response_user.data['access_token'])
        self.client.credentials(HTTP_AUTHORIZATION=self.token)

    def setUp(self):
        # Utils entitys
        self.group_01 = Group.objects.create(name='01', slug='01')
        self.group_02 = Group.objects.create(name='02', slug='02')

    def test_assignment_by_user__success(self):
        """ Test endpoint assign_by_user when user is supervisor. """

        user_data = {
            'username': 'test01',
            'password': '123456.a',
            'level': User.SUPERVISOR
        }
        self.login_user(user_data)
        data_user_target = {
            'username': 'test02',
            'password': '123456.a',
            'level': User.STANDARD
        }
        user_target = User.objects.create_user(**data_user_target)

        post_data = {
            'user': user_target.id,
            'groups': [self.group_01.id, self.group_02.id]
        }
        response = self.client.post('/api/assignments/assign_by_user/', post_data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn(self.group_01, user_target.my_groups.all())

    def test_assignment_by_user__success_standard_users_autoassign_enable(self):
        """ Test endpoint assign_by_user when standard user have permission (auto-assign enable). """

        data_user_allowed = {
            'username': 'test03',
            'password': '123456.a',
            'level': User.STANDARD,
            'has_autoassign': True
        }
        self.login_user(data_user_allowed)

        post_data = {
            'user': self.user_logged.id,
            'groups': [self.group_01.id, self.group_02.id]
        }
        response = self.client.post('/api/assignments/assign_by_user/', post_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn(self.group_01, self.user_logged.my_groups.all())
        self.assertEqual(len(post_data['groups']), self.user_logged.my_groups.count())

    def test_assignment_by_user__error_standard_users_autoassign_disable(self):
        """ Test endpoint assign_by_user when standard user don't have permission (auto-assign disable). """

        data_user_no_allowed = {
            'username': 'test04',
            'password': '123456.a',
            'level': User.STANDARD
        }
        self.login_user(data_user_no_allowed)

        post_data = {
            'user': self.user_logged.id,
            'groups': [self.group_01.id]
        }
        response = self.client.post('/api/assignments/assign_by_user/', post_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_assignment_by_user__error_standard_users_assing_other_user(self):
        """ Test endpoint assign_by_user when standard user assign to other user """

        user_data = {
            'username': 'test05',
            'password': '123456.a',
            'level': User.STANDARD
        }
        self.login_user(user_data)
        data_user_target = {
            'username': 'test06',
            'password': '123456.a',
            'level': User.STANDARD
        }
        user_target = User.objects.create_user(**data_user_target)

        post_data = {
            'user': user_target.id,
            'groups': [self.group_01.id]
        }
        response = self.client.post('/api/assignments/assign_by_user/', post_data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_groups_of_user__success(self):
        """ Test endpoint retrieve for get user's groups with success. """

        data_user = {
            'username': 'test07',
            'password': '123456.a',
            'level': User.SUPERVISOR
        }
        self.login_user(data_user)
        self.user_logged.my_groups.add(self.group_01)

        response = self.client.get('/api/assignments/{}/?object={}'.format(self.user_logged.id, 'user'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.user_logged.my_groups.count(), len(response.data))

    def test_retrieve_users_of_group__success(self):
        """ Test endpoint retrieve for get group's users with success. """

        data_user = {
            'username': 'test08',
            'password': '123456.a',
            'level': User.SUPERVISOR
        }
        self.login_user(data_user)

        # Create alternatives users and assign group
        number_users = 2
        for n in range(number_users):
            data_other_user = {
                'username': 'test' + str(n),
                'password': '123456.a',
                'level': User.STANDARD
            }
            other_user = User.objects.create_user(**data_other_user)
            other_user.my_groups.add(self.group_01)

        # Make request
        response = self.client.get('/api/assignments/{}/?object={}'.format(self.group_01.id, 'group'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(number_users, len(response.data))

    def test_retrieve_users_of_group__error_user_no_allowed(self):
        """ Test endpoint retrieve for get group's users with standard user with auto-assign enable/disable. """

        data_user = {
            'username': 'test09',
            'password': '123456.a',
            'level': User.STANDARD
        }
        self.login_user(data_user)

        # Not allowed
        response = self.client.get('/api/assignments/{}/?object={}'.format(self.group_01.id, 'group'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Allowed
        self.user_logged.has_autoassign = True
        self.user_logged.save()
        response = self.client.get('/api/assignments/{}/?object={}'.format(self.group_01.id, 'group'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_serializer_isolate__error(self):
        """ Testing serializer Assignment_byUserSerializer isolate with an error (endpoint assign_by_user).
            This test is specific for line 30 on serializer in case it is used outside API
            because in the API is checked previously in permissions."""

        user_data = {
            'username': 'test08',
            'password': '123456.a',
            'level': User.STANDARD,
            'has_autoassign': True
        }
        self.login_user(user_data)                     # Just to create user
        data_user_target = {
            'username': 'test09',
            'password': '123456.a',
            'level': User.STANDARD
        }
        user_target = User.objects.create_user(**data_user_target)

        post_data = {
            'user': user_target.id,
            'groups': [self.group_01.id]
        }
        request = type('Expando', (object,), {})()
        request.user = self.user_logged

        serializer = Assignment_byUserSerializer(
            data=post_data,
            context={'request': request}
        )

        self.assertFalse(serializer.is_valid())
        self.assertNotIn(self.group_01, self.user_logged.my_groups.all())

    def test_assignment_by_user__success_test_check_groups(self):
        """ Test check_groups in serializer with success """

        user_data = {
            'username': 'test10',
            'password': '123456.a',
            'level': User.SUPERVISOR
        }
        self.login_user(user_data)
        data_user_target = {
            'username': 'test11',
            'password': '123456.a',
            'level': User.STANDARD
        }
        user_target = User.objects.create_user(**data_user_target)
        user_target.my_groups.add(self.group_01)

        post_data = {
            'user': user_target.id,
            'groups': [self.group_02.id]
        }
        response = self.client.post('/api/assignments/assign_by_user/', post_data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn(self.group_02, user_target.my_groups.all())
        self.assertEqual(2, LogAssignment.objects.filter(assigned_to=user_target).count())
