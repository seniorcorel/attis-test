""" Message prefixes tests. """

# Django
from django.urls import reverse
from django.contrib.auth import get_user_model

# Django REST Framework
from rest_framework import status

# Our
from attis_v2.utils.basetest import BaseAPITestCase
from attis_v2.core.models import MessagePrefix



class MessagePrefixTest(BaseAPITestCase):
    """ Testing Api View and Serializer Model of MessagePrefix Objects. """

    def test_soft_delete__success(self):
        """ Test custom soft delete with success (override perform_destroy method). """

        message = MessagePrefix.objects.create(title='01', body='01')
        self.assertTrue(message.is_active)

        self.login_user({'username': '01', 'password': '123456.a', 'level': get_user_model().SUPERVISOR})
        response = self.client.delete(reverse('api:messageprefix-detail', args=[message.id]))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(MessagePrefix.objects.get(id=message.id).is_active)
        self.assertEqual(1, MessagePrefix.objects.filter(id=message.id).count())

    def test_soft_delete__error_no_allowed(self):
        """ Test custom soft delete with no allowed user (override perform_destroy method). """

        message = MessagePrefix.objects.create(title='01', body='01')

        self.login_user({'username': '01', 'password': '123456.a', 'level': get_user_model().STANDARD})
        response = self.client.delete(reverse('api:messageprefix-detail', args=[message.id]))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrive_no_paginated__success(self):
        """ Test endpoint retrieve with and without no_page parameter, return success. """

        item_by_page = 10
        total_amount = 30
        for i in range(total_amount):
            MessagePrefix.objects.create(title=str(i), body=str(i))

        self.login_user({'username': '01', 'password': '123456.a', 'level': get_user_model().SUPERVISOR})

        response_paginated = self.client.get(reverse('api:messageprefix-list'))
        self.assertEqual(item_by_page, len(response_paginated.data['results']))

        response_no_paginated = self.client.get(reverse('api:messageprefix-list') + '?no_page')
        self.assertEqual(total_amount, len(response_no_paginated.data))


