""" USers's levels permission classes."""

# Django
from django.contrib.auth import get_user_model

# Django REST Framework
from rest_framework.permissions import BasePermission

# Own
from attis_v2.users.models import User


class IsLevelAdmin(BasePermission):
    """Allow access only to user's level is ADMIN."""

    def has_permission(self, request, view):
        """Verify user have rigth level."""
        return request.user.level >= User.ADMIN

class IsLevelSupervisor(BasePermission):
    """Allow access only to user's level is SUPERVISOR."""

    def has_permission(self, request, view):
        """Verify user have rigth level."""
        return request.user.level >= User.SUPERVISOR

class IsLevelStamdard(BasePermission):
    """Allow access only to user's level is STANDARD."""

    def has_permission(self, request, view):
        """Verify user have rigth level."""
        return request.user.level >= User.STANDARD

class AssignGroups(BasePermission):
    """Allow access only to user with SUPERVISOR level or STANDARD with autoassign enable."""

    def has_permission(self, request, view):
        """Verify user have rigth level and if has autoassign"""
        user = request.user
        return (user.level >= User.SUPERVISOR) or (user.level == User.STANDARD and user.has_autoassign)
