"""Api app."""

# Django
from django.apps import AppConfig

class ApiAppConfig(AppConfig):
    """api app config."""

    name = 'attis_v2.api'
    verbose_name = 'Api'
