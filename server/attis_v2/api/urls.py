"""api URLs."""

# Django
from django.urls import include, path
from django.conf.urls import url
from django.views.generic import TemplateView

# Django REST Framework
from rest_framework.routers import DefaultRouter

# Views
from attis_v2.api.views import groups as group_views
from attis_v2.api.views import autorejects as autoreject_views
from attis_v2.api.views import messageprefixes as messageprefix_views
from attis_v2.api.views import messageinternals as messageinternal_views
from attis_v2.api.views import assignments as assignment_views
from attis_v2.api.views import tickets as ticket_views
from attis_v2.api.views import reports as report_views


router = DefaultRouter()
router.register(r'groups', group_views.GroupViewSet, basename='group')
router.register(r'autorejects', autoreject_views.AutorejectViewSet, basename='autoreject')
router.register(r'messageprefixes', messageprefix_views.MessagePrefixViewSet, basename='messageprefix')
router.register(r'messageinternals', messageinternal_views.MessageInternalViewSet, basename='messageinternal')
router.register(r'assignments', assignment_views.AssignmentView, basename='assignment')
router.register(r'tickets', ticket_views.TicketViewSet, basename='ticket')
#router.register(r'reports', report_views.ReportsViewSet, basename='reports')

urlpatterns = [
    # API URLs
    path('', include(router.urls)),
]
