""" Group serializers."""

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import Group

class GroupModelSerializer(serializers.ModelSerializer):
    """ Group model serializer
        Handle data validation and CRUD actions.
    """

    class Meta:
        model = Group
        fields = (
            'id',
            'name',
            'slug',
            'created'
        )

