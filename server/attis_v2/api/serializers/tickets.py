""" Tickets serializers."""

# Django
from django.conf import settings
from django.db import transaction
from django.core.validators import FileExtensionValidator
from django.utils import timezone
from django.db.models import Q

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import Ticket, Group, Domain, FileTicket, Autoreject
from attis_v2.api.serializers import TicketStatusSerializer, AutorejectModelSerializer
from attis_v2.users.models import User
from attis_v2.core.validations import validate_file_maxsize

# Util
from attis_v2.utils import get_md5_file, get_client_ip, get_geo_info, format_error
from django_user_agents.utils import get_user_agent
import datetime

from attis_v2.taskapp.tasks import create_duplicate, new_ticket

class TicketCreateSerializer(serializers.Serializer):
    """ Create ticket serializer
        Handle data validation and create actions.
    """

    id = serializers.CharField(read_only=True)
    account_id = serializers.CharField()
    account_name = serializers.CharField(max_length=50)
    #domain = serializers.CharField()
    #group = serializers.CharField()
    domain = serializers.SlugRelatedField(slug_field='slug', queryset=Domain.objects.all())
    group = serializers.SlugRelatedField(slug_field='slug', queryset=Group.objects.all())
    file = serializers.FileField(
        validators=[
            FileExtensionValidator(allowed_extensions=['pdf', 'png', 'jpg']),
            validate_file_maxsize
        ]
    )

    def validate(self, data):
        """ Validation to create
            - if domain exist
            - if group exist
            - if customer in Autorejects
            - if MD5 exist, return old ticket
            - load geo info and useragent info
        """

        # Check Autoreject
        try:
            autoreject = Autoreject.objects.get(
                Q(expire__gt=timezone.now()) | Q(expire__isnull=True),
                account_id=data['account_id'],
                is_active=True
            )
            raise serializers.ValidationError(format_error("file", "Customer in autoreject", autoreject.message.body))
        except Autoreject.DoesNotExist:
            pass

        # Check  MD5 duplicate
        data['md5'] = get_md5_file(data['file'])
        old_tickets = Ticket.objects.filter(
            account_id=data['account_id'], md5=data['md5']
        ).exclude(
            status=Ticket.TEMP_REJECTED
        )
        if len(old_tickets) > 0:
            create_duplicate.delay(data['account_id'], data['md5'])
            raise serializers.ValidationError(
                format_error("file", "File duplicate", TicketStatusSerializer(old_tickets[0]).data)
            )

        # Check One-ticket-at-a-time
        one_tickets = Ticket.objects.filter(
            account_id=data['account_id'], status__in=[Ticket.PENDING, Ticket.WORKING]
        )
        if len(one_tickets) > 0:
            raise serializers.ValidationError(
                format_error("file", "Already ticket on working", TicketStatusSerializer(one_tickets[0]).data)
            )

        # Load Geo info and UserAgent info
        data['ip'] = get_client_ip(self.context['request'])
        geo_info = get_geo_info(data['ip'])
        data['country'] = geo_info['country_name']
        data['city'] = geo_info['city']
        data['geolocation'] = None if geo_info['latitude'] is None else '{},{}'.format(geo_info['latitude'], geo_info['longitude'])

        user_agent = get_user_agent(self.context['request'])
        data['useragent'] = self.context['request'].META.get('HTTP_USER_AGENT', '')
        data['browser'] = '{} {}'.format(user_agent.browser.family, user_agent.browser.version_string)
        data['os'] = '{} {}'.format(user_agent.os.family, user_agent.os.version_string)
        data['platform'] = 'Mobile' if user_agent.is_mobile else 'Desktop'

        # Create File
        data['file'] = FileTicket.objects.create(file=data['file'])

        return data

    def create(self, data):
        """ Create a ticket object."""

        data['id'] = Ticket.objects.create(**data).id
        transaction.on_commit(lambda: new_ticket.delay(data['id']))
        return data


