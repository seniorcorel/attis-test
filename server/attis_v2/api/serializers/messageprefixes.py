""" Message prefix serializers."""

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import MessagePrefix

class MessagePrefixModelSerializer(serializers.ModelSerializer):
    """ Message prefix model serializer
        Handle data validation and CRUD actions.
    """

    class Meta:
        model = MessagePrefix
        fields = (
            'id',
            'title',
            'body',
            'created'
        )

