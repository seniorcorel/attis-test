""" Group serializers."""

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import FileTicket

class FileTicketModelSerializer(serializers.ModelSerializer):
    """ File ticket model serializer
        Handle data validation and CRUD actions.
    """

    class Meta:
        model = FileTicket
        fields = '__all__'

