""" Autoreject serializers."""

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import Autoreject, MessagePrefix
from attis_v2.api.serializers import MessagePrefixModelSerializer
from attis_v2.utils.serializers import NestedRelatedField

class AutorejectModelSerializer(serializers.ModelSerializer):
    """ Autoreject model serializer
        Handle data validation and CRUD actions.
    """

    message = NestedRelatedField(
        queryset=MessagePrefix.objects.all(),
        model=MessagePrefix,
        serializer_class=MessagePrefixModelSerializer
    )

    class Meta:
        model = Autoreject
        fields = (
            'id',
            'account_id',
            'expire',
            'message',
            'created'
        )

