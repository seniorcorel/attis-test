""" Assignaments serializers."""

# Django Rest Framework
from rest_framework import serializers
from django.contrib.auth import get_user_model

# Own
from attis_v2.core.models import Group, LogAssignment


class Assignment_byUserSerializer(serializers.Serializer):
    """ Assignments of groups by one user
        Handle data validation, assignamet groups and save log assignment
        user_by = loged user
        user_to = user destination
    """

    user = serializers.PrimaryKeyRelatedField(queryset=get_user_model().objects.all())
    groups = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all(), many=True)

    def validate(self, data):
        """ Validations:
            - if user_by is Standard must be user_to is = user_by
        """

        self.user_by = self.context['request'].user
        self.user_to = data['user']

        if self.user_by.level == get_user_model().STANDARD and self.user_by.has_autoassign and self.user_by != self.user_to:
            raise serializers.ValidationError('You do not have permissions for this action')

        return data

    def create(self, data):
        """ Add groups to user_to and save new logassignment. """

        # Update User's groups
        old_groups = list(self.user_to.my_groups.all())
        self.user_to.my_groups.clear()
        self.user_to.my_groups.set(data['groups'])
        self.user_to.save()

        # Add logs
        self.check_groups(old_groups, data['groups'], LogAssignment.REMOVE, self.user_by, self.user_to)
        self.check_groups(data['groups'], old_groups, LogAssignment.ADD, self.user_by, self.user_to)

        return {
            "user": data['user'].id,
            "groups": [group.id for group in data['groups']]
        }

    def check_groups(self, source, destination, type, user_by, user_to):
        """ Check and save log if some group is added or deleted. """
        for item in source:
            if item not in destination:                                                 # pragma: no branch
                LogAssignment.objects.create(type=type, assigned_by=user_by, assigned_to=user_to, group=item)


class LogAssignmentModelSerializer(serializers.ModelSerializer):
    """ Log assignment model serializer
        Handle Read actions.
    """

    class Meta:
        model = LogAssignment
        fields = (
            'id',
            'assigned_by',
            'assigned_to',
            'group',
            'created'
        )


class GroupsByUserSerializer(serializers.Serializer):
    """ Log assignment model serializer
        Handle Read actions.
    """

    class Meta:
        model = LogAssignment
        fields = (
            'id',
            'assigned_by',
            'assigned_to',
            'group',
            'created'
        )
