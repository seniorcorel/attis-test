""" Tickets MODELS serializers."""

# Django
from django.conf import settings

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import Ticket, MessageInternal
from attis_v2.api.serializers import MessagePrefixModelSerializer, GroupModelSerializer, FileTicketModelSerializer, MessageInternalModelSerializer
from attis_v2.users.serializers import UserModelSerializer

class TicketModelSerializer(serializers.ModelSerializer):
    """ Ticket model serializer.
        Handle data validation and read actions
    """

    domain_slug = serializers.ReadOnlyField(source='domain.slug', read_only=True)
    file = FileTicketModelSerializer()
    group = GroupModelSerializer()
    group_name = serializers.ReadOnlyField(source='group.name', read_only=True)
    last_result = MessagePrefixModelSerializer()
    last_user = UserModelSerializer()
    internals = serializers.SerializerMethodField()
    start = serializers.SerializerMethodField()

    class Meta:
        model = Ticket
        fields = '__all__'

    def get_internals(self, obj):
        return MessageInternalModelSerializer(
            MessageInternal.objects.filter(
                account_id=obj.account_id,
                is_active=True),
            many=True
        ).data

    def get_start(self, obj):
        start = (obj.arrived + obj.waittime) if obj.waittime is not None else None
        return DatetimeSerializer({"date": start}).data['date']

class TicketStatusSerializer(serializers.ModelSerializer):
    """ Status ticket serializer
        Structure to serve tickets's status
    """

    last_result = MessagePrefixModelSerializer()

    class Meta:
        model = Ticket
        fields = [
            'id',
            'status',
            'arrived',
            'last_result'
        ]

class TicketInboxSerializer(serializers.ModelSerializer):
    """ Ticket serializer for inbox view
        Structure to serve tickets's status
    """

    group_name = serializers.ReadOnlyField(source='group.name', read_only=True)
    last_user = UserModelSerializer()

    class Meta:
        model = Ticket
        fields = [
            'id',
            'account_id',
            'status',
            'group_name',
            'arrived',
            'waittime',
            'last_user'
        ]

class TicketsByStatusSerializer(serializers.Serializer):
    """ Tickets by status Serializer
        Format data to stats
    """

    status = serializers.IntegerField()
    total = serializers.IntegerField()

class DatetimeSerializer(serializers.Serializer):
    """ Simple serializer to fucking datetime. """
    date = serializers.DateTimeField()
