# """ Reports serializers."""
#
# # Django
#
# # Django Rest Framework
# from rest_framework import serializers
#
# # Own
#
# # Util
#
#
# class TicketsByStatusSerializer(serializers.Serializer):
#     """ Tickets by status Serializer
#         Format data to stats
#     """
#
#     status = serializers.IntegerField()
#     total = serializers.IntegerField()
