""" Message internal serializers."""

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import MessageInternal
from attis_v2.users.serializers import UserModelSerializer


class MessageInternalModelSerializer(serializers.ModelSerializer):
    """ Message internal model serializer
        Handle data validation and CRUD actions.
    """

    # Get and save current user for author
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    # Display username of author
    author_name = serializers.ReadOnlyField(source='author.username', read_only=True)

    class Meta:
        model = MessageInternal
        fields = (
            'id',
            'account_id',
            'username',
            'message',
            'author',
            'author_name',
            'created'
        )

