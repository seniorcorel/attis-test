""" Taken MODELS serializers."""

# Django
from django.conf import settings

# Django Rest Framework
from rest_framework import serializers

# Own
from attis_v2.core.models import Taken

class TakenModelSerializer(serializers.ModelSerializer):
    """ Taken model serializer.
        Handle data validation and read actions
    """

    class Meta:
        model = Taken
        fields = ('start', 'end')
