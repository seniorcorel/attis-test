"""Core admin."""

# Django
from django.contrib import admin
from django.db import models

# Models
from attis_v2.core.models import *

# Python
from datetime import datetime


@admin.register(Autoreject)
class AutorejectAdmin(admin.ModelAdmin):
    list_display = ('id', 'account_id', 'expire', 'is_active')
    search_fields = ('id', 'account_id')
    list_filter = ('created', 'modified')


@admin.register(Domain)
class DomainAdmin(admin.ModelAdmin):
    list_display = ('id', 'slug', 'is_active')
    search_fields = ('id', 'slug')
    list_filter = ('created', 'modified')


@admin.register(Duplicate)
class DuplicateAdmin(admin.ModelAdmin):
    list_display = ('id', 'account_id')
    search_fields = ('id', 'account_id')
    list_filter = ('created', 'modified')


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created', 'is_active')
    search_fields = ('id', 'name')
    list_filter = ('created', 'modified')


@admin.register(MessagePrefix)
class MessagePrefixAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'body', 'created', 'is_active')
    search_fields = ('id', 'title', 'body')
    list_filter = ('created', 'modified')


@admin.register(Taken)
class TakenAdmin(admin.ModelAdmin):
    list_display = ('id', 'ticket', 'start', 'end', 'duration', 'status', 'user')
    search_fields = ('id', 'ticket', 'status', 'user')
    list_filter = ('status', 'created', 'modified')


def reset_ticket(self, request, queryset):
    for obj in queryset:
        Taken.objects.filter(ticket=obj.id).delete()
        queryset.update(
            status=0,
            arrived=datetime.now(),
            waittime=None,
            last_duration=None
        )


reset_ticket.short_description = 'Reset ticket'


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'account_id', 'account_name', 'domain', 'group', 'status', 'arrived')
    search_fields = ('id', 'account_id', 'account_name', 'group')
    list_filter = ('domain', 'status', 'group', 'arrived')
    actions = [reset_ticket, ]


@admin.register(FileTicket)
class FileTicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'file')
    search_fields = ('id', 'file')


@admin.register(Queue)
class QueueAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('id', 'name')
