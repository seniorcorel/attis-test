# Generated by Django 2.2.7 on 2020-03-18 16:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_ticket_is_served'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='browser',
            field=models.CharField(help_text='Customer request browser', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='os',
            field=models.CharField(help_text='Customer request OS', max_length=100, null=True),
        ),
    ]
