"""Core app."""

# Django
from django.apps import AppConfig

class CoreAppConfig(AppConfig):
    """Core app config."""

    name = 'attis_v2.core'
    verbose_name = 'Core'
