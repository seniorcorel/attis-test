""" Tickets model. """

# Django
from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator
from django.db import transaction
from django.utils import timezone

# Own
from attis_v2.utils.basemodel import BaseModel
from attis_v2.core.models import Domain, Group, MessagePrefix
from attis_v2.core.validations import validate_file_maxsize

# Util
import uuid
import os
from datetime import datetime

class TicketManager(models.Manager):
    """Manager to add extra functions to tickets. """

    def next(self, groups):
        with transaction.atomic():
            ticket = super().get_queryset()\
                .filter(status=Ticket.PENDING, is_served=False, group__id__in=groups)\
                .order_by('created')\
                .select_for_update()\
                .first()

            if ticket is not None:
                ticket.is_served = True
                ticket.save()
        return ticket

    def rollback(self, ticket):
        ticket.is_served = False
        ticket.save()

class FileTicket(BaseModel):
    def rename_uuid(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('upload', filename)

    file = models.FileField(
        upload_to=rename_uuid,
        validators=[
            FileExtensionValidator(allowed_extensions=['pdf', 'png', 'jpg']),
            validate_file_maxsize
        ]
    )

    def __str__(self):
        """Return filename."""
        return str(self.file.name)


class Ticket(BaseModel):
    """ Release request. """

    PENDING = 0
    WORKING = 1
    SUCCESS = 2
    REJECTED = 3
    TRANSFERRED = 4
    AUTOREJECTED = 5
    TEMP_REJECTED = 6
    TICKET_STATUS = [
        (PENDING, 'Pending'),
        (WORKING, 'Working'),
        (SUCCESS, 'Success'),
        (REJECTED, 'Rejected'),
        (TRANSFERRED, 'Transferred'),
        (AUTOREJECTED, 'AutoRejected'),
        (TEMP_REJECTED, 'Temporary Rejected'),
    ]

    # Base Info -------------------------------------*
    id = models.BigAutoField(
        unique=True,
        primary_key=True
    )
    account_id = models.BigIntegerField(
        'Account ID',
        help_text="Customer's ID"
    )
    account_name = models.CharField(
        max_length=50,
        help_text="Customer's name"
    )
    domain = models.ForeignKey(
        Domain,
        on_delete=models.PROTECT,
        related_name='tickets'
    )
    group = models.ForeignKey(
        Group,
        on_delete=models.PROTECT,
        related_name='tickets'
    )
    # file = models.FileField(
    #     upload_to='uploads/',
    #     validators=[
    #         FileExtensionValidator(allowed_extensions=['pdf', 'png', 'jpg']),
    #         validate_file_maxsize
    #     ]
    # )
    file = models.ForeignKey(
        FileTicket,
        on_delete=models.PROTECT,
        related_name='tickets'
    )

    md5 = models.CharField(
        max_length=40,
        help_text='MD5 of file'
    )
    arrived = models.DateTimeField(
        auto_now_add=True,
        help_text='Date time on which the ticket was arrived'
    )

    # Result Info -------------------------------------*
    waittime = models.DurationField(
        help_text='Waiting time before being assigned to an agent',
        null=True
    )
    status = models.PositiveSmallIntegerField(
        choices=TICKET_STATUS,
        default=PENDING,
        help_text="Current ticket's status",
        db_index=True
    )
    last_result = models.ForeignKey(
        MessagePrefix,
        on_delete=models.PROTECT,
        related_name='tickets',
        null=True
    )
    last_user = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
        related_name='tickets',
        null=True
    )
    last_duration = models.DurationField(
        null=True,
        help_text="Working time of the ticket"
    )

    # Request -------------------------------------*
    ip = models.GenericIPAddressField(
        help_text='Customer request IP',
        null=True
    )
    country = models.CharField(
        max_length=50,
        help_text='Customer request country',
        null=True
    )
    city = models.CharField(
        max_length=50,
        help_text='Customer request city',
        null=True
    )
    geolocation = models.CharField(
        max_length=50,
        help_text='Customer request geolocation',
        null=True
    )
    useragent = models.CharField(
        max_length=500,
        help_text='Customer request useragent',
        null=True
    )
    platform = models.CharField(
        max_length=5100,
        help_text='Customer request platform',
        null=True
    )
    os = models.CharField(
        max_length=100,
        help_text='Customer request OS',
        null=True
    )
    browser = models.CharField(
        max_length=100,
        help_text='Customer request browser',
        null=True
    )
    is_served = models.BooleanField(
        default=False,
        help_text='Indicates if the ticket is served'
    )

    def __str__(self):
        """Return ticket ID."""
        return str(self.id)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

    objects = models.Manager()
    fifo_objects = TicketManager()


