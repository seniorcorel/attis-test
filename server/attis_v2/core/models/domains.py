""" Domains model. """

# Django
from django.db import models

# Own
from attis_v2.utils.basemodel import BaseModel

class Domain(BaseModel):
    """ Source domains. """

    slug = models.SlugField(
        max_length=20,
        help_text='Doamin slug for URLs'
    )
    is_active = models.BooleanField(
        'active status',
        default=True
    )

    def __str__(self):
        """Return group name."""
        return self.slug

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

