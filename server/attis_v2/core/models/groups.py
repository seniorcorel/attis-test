""" Groups model. """

# Django
from django.db import models
from django.contrib.auth import get_user_model

# Own
from attis_v2.utils.basemodel import BaseModel

class Group(BaseModel):
    """ Group to segment to customer. """

    name = models.CharField(
        max_length=100,
        help_text='Name of the group'
    )
    slug = models.SlugField(
        max_length=20,
        help_text='Group slug for URLs',
        null=True,
        unique=True
    )
    is_active = models.BooleanField(
        'active status',
        default=True
    )

    users = models.ManyToManyField(get_user_model(), related_name='my_groups', blank=True)

    def __str__(self):
        """Return group name."""
        return self.name

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

