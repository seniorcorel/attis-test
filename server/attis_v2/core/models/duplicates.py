""" Duplicate model."""

# Django
from django.db import models

# Own
from attis_v2.utils.basemodel import BaseModel

class Duplicate(BaseModel):
    """ Ticket attempt with duplicate files (based on file MD5). """

    account_id = models.BigIntegerField(
        'Account ID',
        help_text="Customer's ID"
    )
    md5 = models.CharField(
        max_length=100,
        help_text='MD5 of file'
    )

       
    def __str__(self):
        """Return group name."""
        return "{}: {}".format(self.account_id, self.md5)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

