"""Message Internal model."""

# Django
from django.db import models
from django.contrib.auth import get_user_model

# Own
from attis_v2.utils.basemodel import BaseModel

class MessageInternal(BaseModel):
    """ Messages Internal between users to help release tickets """

    account_id = models.BigIntegerField(
        'Account ID',
        help_text="Customer's ID"
    )

    username = models.TextField(
        max_length=150,
        null=True
    )

    message = models.TextField(
        max_length=512,
        help_text='Message to customer'
    )
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
        related_name='messageinternals',
        null=True
    )
    is_active = models.BooleanField(
        'active status',
        default=True
    )

    def __str__(self):
        """Return message title."""
        return "Message for {}".format(self.account_id)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

