"""Log Assignment model."""

# Django
from django.db import models
from django.contrib.auth import get_user_model

# Own
from attis_v2.utils.basemodel import BaseModel
from attis_v2.core.models import Group

class LogAssignment(BaseModel):
    """ Log of assignments between User and Groups. """

    ADD='A'
    REMOVE='R'
    ASSIGN_TYPE = [
        (ADD, 'Added'),
        (REMOVE, 'Removed')
    ]

    type = models.CharField(
        max_length=1,
        help_text='A if was added and R if was removed',
        choices=ASSIGN_TYPE,
        default=ADD
    )
    assigned_by = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='my_assignmetslogs')
    assigned_to = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='assignmetslogs')
    group = models.ForeignKey(Group, on_delete=models.PROTECT, related_name='assignmetslogs')

       
    def __str__(self):
        """Return summary."""
        return "{} assign {} to {}".format(self.assigned_by, self.group, self.assigned_to)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

