""" Queues model. """

# Django
from django.db import models
from django.contrib.auth import get_user_model

# Own
from attis_v2.utils.basemodel import BaseModel
from attis_v2.core.models import Group

class Queue(BaseModel):
    """ Queue to segment the priority of ticket. """

    name = models.CharField(
        max_length=50,
        help_text='Name of the queue'
    )
    groups = models.ManyToManyField(Group, related_name='queues', blank=True)
    is_active = models.BooleanField(
        'active status',
        default=True
    )

    def __str__(self):
        """Return queue name."""
        return self.name

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

