"""Summary Status model."""

# Django
from django.db import models
from django.contrib.auth import get_user_model

# Own
from attis_v2.utils.basemodel import BaseModel
from attis_v2.users.models import User

class SummaryStatus(BaseModel):
    """ Summery of logs of changes of User's status """

    duration = models.DurationField(help_text='Duration of status')
    status = models.PositiveIntegerField(choices=User.USER_STATUS, default=User.BACK_5)
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='summary_status')


    def __str__(self):
        """Return summary."""
        return "{} change to status {}".format(self.user, self.status)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

