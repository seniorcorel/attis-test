"""Message Prefix model."""

# Django
from django.db import models

# Own
from attis_v2.utils.basemodel import BaseModel

class MessagePrefix(BaseModel):
    """ Messages Prefixed to respond tickets """

    title = models.CharField(
        max_length=100,
        help_text='Title of the message'
    )
    body = models.TextField(
        max_length=512,
        help_text='Message to customer'
    )
    is_active = models.BooleanField(
        'active status',
        default=True
    )

    def __str__(self):
        """Return message title."""
        return self.title

    class Meta:
        """Meta class."""
        ordering = ['title']

