""" Taken model. """

# Django
from django.db import models
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError

# Own
from attis_v2.utils.basemodel import BaseModel
from attis_v2.core.models import Ticket, MessagePrefix

import logging
logger = logging.getLogger(__name__)

class Taken(BaseModel):
    """ Relation between Ticket and User. """

    start = models.DateTimeField(
        help_text='User start work with ticket'
    )
    end = models.DateTimeField(
        null=True,
        help_text='User end work with ticket'
    )
    duration = models.DurationField(
        null=True,
        help_text="Working time of the ticket"
    )
    status = models.PositiveSmallIntegerField(
        choices=Ticket.TICKET_STATUS,
        default=Ticket.PENDING,
        help_text="Current ticket's status"
    )
    ticket = models.ForeignKey(
        Ticket,
        on_delete=models.PROTECT,
        related_name='taken'
    )
    result = models.ForeignKey(
        MessagePrefix,
        on_delete=models.PROTECT,
        related_name='taken',
        null=True
    )
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
        related_name='taken',
        null=True
    )

    def __str__(self):
        """Return summary."""
        return "{} worked by {}".format(self.id, self.user)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

    """def save(self, *args, **kwargs):
        if self.id is None:
            amount = Taken.objects.filter(user=self.user, status=Ticket.WORKING).count()
            logger.warning("{} - Clean {} - {}".format(self.id, self.user, amount))
            if amount >= 3:
                raise ValidationError('Max number of simultaneous')

        return super(Taken, self).save(*args, **kwargs)
    """

