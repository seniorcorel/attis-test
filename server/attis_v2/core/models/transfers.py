""" Transfer model. """

# Django
from django.db import models
from django.contrib.auth import get_user_model

# Own
from attis_v2.utils.basemodel import BaseModel
from attis_v2.core.models import Taken

class Transfer(BaseModel):
    """ Ticket transfer record """

    user_from = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
        related_name='transfer_from',
        help_text='Source user'
    )
    user_to = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
        related_name='transfer_to',
        help_text='Destination user'
    )
    user_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
        related_name='transfer_by',
        help_text='Executing user'
    )

    taken_old = models.ForeignKey(
        Taken,
        on_delete=models.PROTECT,
        related_name='transfer_source',
    )

    taken_new = models.ForeignKey(
        Taken,
        on_delete=models.PROTECT,
        related_name='transfer_destination',
    )
       
    def __str__(self):
        """Return summary."""
        return "{} to {} - New taken: {}".format(self.user_from, self.user_to, self.taken_new.id)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

