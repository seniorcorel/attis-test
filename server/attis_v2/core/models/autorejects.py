""" Autoreject model. """

# Django
from django.db import models

# Own
from attis_v2.utils.basemodel import BaseModel
from attis_v2.core.models import MessagePrefix

class Autoreject(BaseModel):
    """ Customer in blacklist to set the tickets as autorejected. """

    account_id = models.BigIntegerField(
        'Account ID',
        help_text="Customer's ID"
    )
    expire = models.DateTimeField(
        help_text='Date when user get out of blacklist',
        null=True
    )
    message = models.ForeignKey(
        MessagePrefix,
        on_delete=models.PROTECT,
        related_name='autoreject'
    )
    is_active = models.BooleanField(
        'active status',
        default=True
    )
       
    def __str__(self):
        """Return account id."""
        return str(self.account_id)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

