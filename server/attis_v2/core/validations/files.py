from django.core.exceptions import ValidationError
from django.conf import settings

def validate_file_maxsize(value):
    """ Validate max size of file. """
    filesize = value.size

    maxsize = int(settings.FILE_MAXSIZE_MB) * 1e6
    if filesize > maxsize:
        raise ValidationError("The maximum file size that can be uploaded is {}MB".format(settings.FILE_MAXSIZE_MB))
    else:
        return value
