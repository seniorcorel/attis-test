""" Tickets Consumer for websocket. """

from django.conf import settings
from django.utils import timezone
from django.db.models import Count
from django.db import transaction

# Channels
from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer, AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async

# Owm
from attis_v2.websocket.utils import ClientError
from attis_v2.core.models import Ticket, MessagePrefix, MessageInternal, Taken, Transfer
from attis_v2.users.models import User
from attis_v2.api.serializers import TicketInboxSerializer, TicketModelSerializer, MessagePrefixModelSerializer, MessageInternalModelSerializer, TicketsByStatusSerializer
from attis_v2.users.serializers import UserModelSerializer
from attis_v2.taskapp.tasks import save_ticket, serve_ticket


from datetime import datetime


class TicketsConsumer(AsyncJsonWebsocketConsumer):
    """ This consumer handles websocket connections for agents clients. """

    GROUP_ALL = 'all'

    async def connect(self):
        """ Called when the websocket is handshaking as part of initial connection. """


        # Are they logged in?
        if self.scope["user"].is_anonymous:
            # Reject the connection
            await self.close()
        else:
            self.room_name = 'user_' + str(self.scope["user"].id)
            await self.channel_layer.group_add(
                self.room_name,
                self.channel_name
            )

            await self.channel_layer.group_add(
                self.GROUP_ALL,
                self.channel_name,
            )
            # Accept the connection
            await self.accept()

    async def disconnect(self, code):
        """ Called when the WebSocket closes for any reason. """

        # Leave all the rooms we are still in
        try:
            await self.channel_layer.group_discard(
                self.room_name,
                self.channel_name,
            )
            await self.channel_layer.group_discard(
                self.GROUP_ALL,
                self.channel_name,
            )
        except AttributeError:
            pass

    async def receive_json(self, content):
        """
        Called when we get a text frame. Channels will JSON-decode the payload
        for us and pass it as the first argument.
        """

        # Messages will have a "command" key we can switch on
        command = content.get("command", None)
        data = content.get("data", None)
        try:
            if command == settings.CHANNEL_COMMANDS['GET_INBOX']:
                await self.get_inbox(command)
            elif command == settings.CHANNEL_COMMANDS['SAVE_TICKET']:
                await self.save_ticket(command, data)
            elif command == settings.CHANNEL_COMMANDS['SERVE']:
                await self.serve(command)
            elif command == settings.CHANNEL_COMMANDS['GET_INTERNAL']:
                await self.get_internal(command, data)
            elif command == settings.CHANNEL_COMMANDS['TRANSFER_TICKET']:
                await self.transfer_ticket(command, data)
            elif command == settings.CHANNEL_COMMANDS['GET_USERS_ONLINE']:
                await self.get_users_online(command)
            elif command == settings.CHANNEL_COMMANDS['GET_TICKET']:
                await self.get_ticket(command, data)
            elif command == settings.CHANNEL_COMMANDS['GET_TICKET_BY_STATUS']:
                await self.get_ticket_by_status(command, data)
        except ClientError as e:
            # Catch any errors and send it back
            await self.send_json({"error": e.code})



    async def send_all(self, command, data):
        await self.channel_layer.group_send(
            self.GROUP_ALL,
            {
                'type': 'send_json',
                'message': {
                    'command': command,
                    'data': data
                }
            }
        )

    async def send_user(self, command, data, user=None):
        user = self.scope["user"].id if user is None else user
        target = 'user_' + str(user)
        await self.channel_layer.group_send(
            target,
            {
                'type': 'send_json',
                'message': {
                    'command': command,
                    'data': data
                }
            }
        )

    async def send_toast(self, level, message, user=None):
        body = {
            'level': level,
            'message': message
        }
        if user is None:
            await self.send_all(settings.CHANNEL_COMMANDS['TOAST'], body)
        else:
            await self.send_user(settings.CHANNEL_COMMANDS['TOAST'], body, user)

    async def get_inbox(self, command):
        inbox = await self.get_tickets_for_inbox()
        inbox_json = TicketInboxSerializer(inbox, many=True).data

        working = await self.get_tickets_working(self.scope["user"])
        working_json = TicketModelSerializer(working, many=True).data

        messages = await self.get_message_prefixes()
        messages_json = MessagePrefixModelSerializer(messages, many=True).data
        data = {
            'inbox': inbox_json,
            'working': working_json,
            'messages': messages_json
        }
        await self.send_user(command, data)

    async def save_ticket(self, command, data):
        data['user'] = self.scope["user"].id
        save_ticket.delay(data)
        #await self.send_user(command, data)

    async def serve(self, command):
        serve_ticket.delay(None, self.scope["user"].id)

    async def get_internal(self, command, data):
        internal = await self.get_message_prefixes(data)
        internal_json = MessageInternalModelSerializer(internal, many=True).data
        await self.send_user(command, internal_json)

    async def transfer_ticket(self, command, data):
        user_by = self.scope["user"]
        user_to_id = data['user']
        ticket_id = data['ticket']

        try:
            ticket, user_from_id = await self.save_tranfer(ticket_id, user_to_id, user_by)
            ticket_json = TicketModelSerializer(ticket).data

            # Notify inbox
            await self.send_all(settings.CHANNEL_COMMANDS['UPDATE_TICKET'], ticket_json)

            # Notify old user
            await self.send_user(settings.CHANNEL_COMMANDS['SAVE_TICKET_SUCCESS'], ticket_json, user_from_id)

            # Notify current user
            if user_by.id != user_from_id:
                await self.send_user(settings.CHANNEL_COMMANDS['SAVE_TICKET_SUCCESS'], ticket_json, user_by.id)

            # Notify new user
            await self.send_user(settings.CHANNEL_COMMANDS['WORK_TICKET'], ticket_json, user_to_id)
        except Exception as e:
            await self.send_toast("E", str(e), user_by.id)

    async def get_users_online(self, command):
        online = await self.get_online()
        online_json = UserModelSerializer(online, many=True).data
        await self.send_user(command, online_json)

    async def get_ticket(self, command, data):
        ticket = await self.get_ticket_object(data['ticket'])
        ticket_json = TicketModelSerializer(ticket).data
        await self.send_user(command, ticket_json)

    async def get_ticket_by_status(self, command, data):
        tickets = await self.get_ticket_by_status_object(data['account_id'])
        result = {
            'ticket': data['ticket'],
            'data': TicketsByStatusSerializer(tickets, many=True).data
        }
        await self.send_user(command, result)

    @database_sync_to_async
    def get_tickets_for_inbox(self):
        return Ticket.objects.filter(status__lte=Ticket.WORKING).order_by('id')

    @database_sync_to_async
    def get_tickets_working(self, user):
        return Ticket.objects.filter(status=Ticket.WORKING, last_user=user)

    @database_sync_to_async
    def get_message_prefixes(self):
        return MessagePrefix.objects.filter(is_active=True)

    @database_sync_to_async
    def get_message_internals(self, account_id):
        return MessageInternal.objects.filter(account_id=account_id, is_active=True)

    @database_sync_to_async
    def save_tranfer(self, ticket_id, user_to_id, user_by):
        now = datetime.now(timezone.utc)
        user_to = User.objects.get(id=user_to_id)

        try:
            with transaction.atomic():
                ticket = Ticket.objects.get(id=ticket_id)
                user_from = ticket.last_user
                ticket.last_user = user_to
                ticket.save()

                taken_old = Taken.objects.get(ticket=ticket, status=Ticket.WORKING)
                taken_old.end = now
                taken_old.status = Ticket.TRANSFERRED
                taken_old.duration = now - taken_old.start
                taken_old.save()

                taken_new = Taken.objects.create(
                    start=now,
                    status=Ticket.WORKING,
                    ticket=ticket,
                    user=user_to
                )

                tranfer = Transfer.objects.create(
                    user_from=user_from,
                    user_to=user_to,
                    user_by=user_by,
                    taken_old=taken_old,
                    taken_new=taken_new
                )

                return ticket, user_from.id
        except Exception as e:
            if 'Maximum simultaneous ticket reached' in str(e):
                raise Exception('Agent reached the maximum simultaneous ticket')
            else:
                raise e

    @database_sync_to_async
    def get_online(self):
        return User.objects.filter(status=User.ONLINE, is_staff=False) #.exclude(id=self.scope["user"].id)

    @database_sync_to_async
    def get_ticket_object(self, ticket_id):
        try:
            return Ticket.objects.get(id=ticket_id)
        except Ticket.DoesNotExist:
            return None

    @database_sync_to_async
    def get_ticket_by_status_object(self, account_id):
        return Ticket.objects.filter(account_id=account_id, status__gt=Ticket.WORKING).\
            values('status').\
            annotate(total=Count('status')).\
            order_by('status')
