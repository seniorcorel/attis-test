""" Websockets routing """
from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from attis_v2.websocket.utils import TokenAuthMiddleware
from attis_v2.websocket.consumers import tickets

application = ProtocolTypeRouter({
    'websocket': TokenAuthMiddleware(
        URLRouter(
            [
                url(r'^ws/tickets/$', tickets.TicketsConsumer),
            ]
        )
    ),
})
