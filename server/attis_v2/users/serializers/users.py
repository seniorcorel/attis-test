"""Users serializers."""

# Django
from django.conf import settings
from django.contrib.auth import authenticate

# Django REST Framework
from rest_framework import serializers
from rest_framework.authtoken.models import Token

# Own
from attis_v2.users.models import User
from attis_v2.core.models import Ticket

# Util
from datetime import datetime
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

class UserModelSerializer(serializers.ModelSerializer):
    """User model serializer."""

    class Meta:
        """Meta class."""

        model = User
        fields = (
            'id',
            'username',
            'level',
            'status',
            'has_autoassign'
        )


class UserTokenSerializer(serializers.Serializer):
    """User Token Serializer.

    Handles the Office 365 login request data.
    raises:
        ValidationError: if the credentials are not good: password must be a
        valid token, to call Microsoft API and get the user, same provided as
        username.
    """
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        """Check credentials"""
        data['username'] = data['username'].lower()

        user = authenticate(username=data['username'],
                            password=data['password'])
        if not user:
            raise serializers.ValidationError('Invalid credentials')
        self.context['user'] = user
        return data

    def create(self, data):
        """Generate or retrieve new token."""

        self.context['user'].status = User.BACK_5
        self.context['user'].save()

        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key


class UserLoginSerializer(serializers.Serializer):
    """User login serializer.

    Handle the login request data.
    """

    username = serializers.CharField()
    password = serializers.CharField(min_length=8, max_length=64)

    def validate(self, data):
        """Check credentials."""
        data['username'] = data['username'].lower()

        user = authenticate(username=data['username'], password=data['password'])
        if not user:
            raise serializers.ValidationError('Invalid credentials')
        self.context['user'] = user
        return data

    def create(self, data):
        """Generate or retrieve new token."""

        self.context['user'].status = User.BACK_5
        self.context['user'].save()

        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key


class UserNowSerializer(serializers.ModelSerializer):
    """User model serializer to User Now report."""

    ticket_today = serializers.SerializerMethodField()

    class Meta:
        """Meta class."""

        model = User
        fields = (
            'id',
            'username',
            'level',
            'status',
            'has_autoassign',
            'ticket_today'
        )

    def get_ticket_today(self, item):
        return Ticket.objects.filter(last_user=item, arrived__date=datetime.today()).count()


class UserUpdateModelSerializer(serializers.ModelSerializer):
    """User model serializer to update specific fields."""

    class Meta:
        """Meta class."""

        model = User
        fields = ('has_autoassign',)


class ForceLogoutSerializer(serializers.Serializer):
    """Force logout user serializer.

    Handle the force logout from client.
    """

    target_user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    def create(self, data):
        """ Change status of target and notifiy them."""

        target = data['target_user']
        target.status = User.OFFLINE
        target.save()

        # Notify target
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'user_' + str(target.id),
            {
                'type': 'send_json',
                'message': {'command': settings.CHANNEL_COMMANDS['FORCE_LOGOUT'], 'data': {}}
            }
        )

        # Notify trigger
        """trigger = self.context['request'].user
        message = {
            'type': 'success',
            'message': 'User successfully logged out'
        }
        self.send_to_client('user_' + str(trigger.id), settings.CHANNEL_COMMANDS['TOAST'], message)
        """
        return {'target_user': target.id}
