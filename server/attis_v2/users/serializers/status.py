"""Status serializers."""

# Django
from django.conf import settings

# Django REST Framework
from rest_framework import serializers

# Models
from attis_v2.users.models import User
from attis_v2.core.models import LogStatus, SummaryStatus

# Util
from datetime import datetime, timezone


class StatusChangeSerializer(serializers.Serializer):
    """ Status Change serializer.

    Handle users's status update and save logs.
    """

    status = serializers.ChoiceField(choices=User.USER_STATUS)

    def validate(self, data):
        """ Check if exist other user on status online """
        self.user = self.context['request'].user
        if data['status'] != User.ONLINE:
            if User.objects.exclude(id=self.user.id).filter(status=User.ONLINE, level__lte=User.SUPERVISOR).count() == 0:
                raise serializers.ValidationError('You cannot change status because there is no other agent online.')

        return data

    def create(self, data):
        """ Update user's status and save logs"""

        try:
            self.user.status = data['status']
            self.user.save()

            self.create_summary(self.user)

            LogStatus.objects.create(
                user=self.user,
                status=data['status']
            )
        except Exception as e:
            raise serializers.ValidationError('Error saving status: {}'.format(str(e)))
        return data


    def create_summary(self, user):
        try:
            last_status = LogStatus.objects.filter(user=user).order_by('-id')[:1][0]

            duration = datetime.now(timezone.utc) - last_status.created
            SummaryStatus.objects.create(
                user=user,
                status=last_status.status,
                duration=duration
            )
        except IndexError:
            pass # If there aren't LogStatus for this user then summary is unnecessary

