"""Users views."""

# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

# Permissions
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from attis_v2.api.permissions import AssignGroups

# Serializers
from attis_v2.users.serializers import (
    UserLoginSerializer,
    UserTokenSerializer,
    UserModelSerializer,
    StatusChangeSerializer,
    UserNowSerializer,
    UserUpdateModelSerializer,
    ForceLogoutSerializer
)

# Models
from attis_v2.users.models import User

class UserViewSet(  mixins.RetrieveModelMixin,
                    mixins.ListModelMixin,
                    mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    """User view set.
    Handle login.
    """

    queryset = User.objects.filter(is_active=True, is_staff=False)

    def get_serializer_class(self):
        if self.action in ['update']:
            return UserUpdateModelSerializer
        return UserModelSerializer

    def get_permissions(self):
        """Assign permissions based on action."""
        permissions = []
        if self.action in ['login', 'token']:
            permissions = [AllowAny]
        else:
            permissions = [IsAuthenticated]
        return [p() for p in permissions]

    def get_queryset(self):
        """ Return current user only if is List action and user is a standard. """
        user = self.request.user
        if self.action in ['list'] and (user.level == User.STANDARD):
            return User.objects.filter(is_active=True, is_staff=False, id=user.id)
        else:
            return User.objects.filter(is_active=True, is_staff=False)

    @action(detail=False, methods=['post'])
    def token(self, request):
        """
        User token login
        params:
            request: obj -> the API request
        returns:
            Response with status code 201 if User validations succeded
        raises:
            exception if serializer raise ValidationError
        """
        serializer = UserTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        data = {
            'user': UserModelSerializer(user).data,
            'access_token': token
        }
        return Response(data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def login(self, request):
        """User sign in."""
        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        data = {
            'user': UserModelSerializer(user).data,
            'access_token': token
        }
        return Response(data, status=status.HTTP_201_CREATED)

    def paginate_queryset(self, queryset):
        """ Necessary filter to obtain all the records without page (it is useful to list these
        objects in selects in frontend). """
        if 'no_page' in self.request.query_params:
            return None
        return super().paginate_queryset(queryset)

    @action(detail=False, methods=['post'])
    def change_status(self, request):
        """ Update user's status """
        serializer = StatusChangeSerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        data = serializer.save()

        return Response(data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['get'])
    def now(self, request):
        """ List of users with current info """
        users = User.objects.filter(level__lte=User.SUPERVISOR, is_staff=False).order_by('status')
        data = UserNowSerializer(users, many=True).data
        return Response(data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def force_logout(self, request, *args, **kwargs):
        """ Force logout and update status. """
        serializer = ForceLogoutSerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        return Response(data, status=status.HTTP_200_OK)


