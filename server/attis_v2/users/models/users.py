"""User model."""

# Django
from django.db import models
from django.contrib.auth.models import AbstractUser

# Own


class User(AbstractUser):
    """User model.
    Extend from Django's Abstract User, add some extra fields.
    """


    STANDARD = 0
    SUPERVISOR = 1
    ADMIN = 2
    USER_LEVELS = [
        (STANDARD, 'Standard'),
        (SUPERVISOR, 'Supervisor'),
        (ADMIN, 'Admin'),
    ]

    
    ONLINE = 0
    BACK_5 = 1
    BREAK = 2
    PRELOGOFF = 3
    OFFLINE = 4
    USER_STATUS = [
        (ONLINE, 'ONLINE'),
        (BACK_5, 'BACK_5'),
        (BREAK, 'BREAK'),
        (PRELOGOFF, 'PRELOGOFF'),
        (OFFLINE, 'OFFLINE'),
    ]

    level = models.PositiveIntegerField(
        choices=USER_LEVELS,
        default=STANDARD,
        help_text='Level of authorization'
    )

    status = models.PositiveIntegerField(
        choices=USER_STATUS,
        default=OFFLINE,
        help_text='Current status of user'
    )

    has_autoassign = models.BooleanField(
        'auto assing',
        default=False,
        help_text='Set if the user can auto assign groups'
    )



    def __str__(self):
        """Return username."""
        return self.username

    def get_short_name(self):
        """Return username."""
        return self.username
