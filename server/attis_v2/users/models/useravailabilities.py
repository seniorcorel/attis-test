""" User Availability model. """

# Django
from django.db import models

# Own
from attis_v2.utils.basemodel import BaseModel

class UserAvailability(BaseModel):
    """ Model that saves the total availability of users in the system
    Availability formule: look get_index_user_availability function. """

    tickets_working = models.IntegerField(
        help_text='Current tickets on Working status'
    )
    users_online = models.IntegerField(
        help_text='Current users on Online status'
    )
    availability = models.FloatField(
        help_text='Index User availability'
    )

    def __str__(self):
        """Return user availability."""
        return "{}: {}%".format(self.created, self.availability)

    class Meta:
        """Meta class."""
        ordering = ['-created', '-modified']

