"""User models admin."""

# Django
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

# Models
from attis_v2.users.models import User


class UserAdmin(BaseUserAdmin):
    """User model admin."""

    list_display = ('id', 'username', 'level', 'has_autoassign')
    list_display_links = ('username',)
    list_filter = ('level', 'status', 'has_autoassign')
    fieldsets = BaseUserAdmin.fieldsets + (
        ('Attis Fields', {'fields': ('level', 'has_autoassign', 'status')}),
    )

admin.site.register(User, UserAdmin)
