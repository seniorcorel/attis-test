"""Celery tasks."""

# Django
from django.conf import settings
from django.utils import timezone
from django.db import transaction
from django.db import connection

# Own
from attis_v2.core.models import Ticket, Group, Domain, FileTicket, Duplicate, Taken, MessagePrefix, Queue
from attis_v2.users.models import User, UserAvailability
from attis_v2.api.serializers import TicketInboxSerializer, TicketModelSerializer, TakenModelSerializer
from attis_v2.utils import get_index_user_availability

# Utilities
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import logging
from datetime import datetime
import random
from datetime import timedelta
import time

# Celery
from celery.decorators import task, periodic_task

# Get an instance of a logger
logger = logging.getLogger(__name__)


@task(name='create_ticket', max_retries=3)
def create_ticket(data):
    """ Create new ticket. """
    print(data)

    data['domain'] = Domain.objects.get(slug=data['domain'])
    data['group'] = Group.objects.get(id=data['group'])
    data['file'] = FileTicket.objects.get(id=data['file'])

    return Ticket.objects.create(**data).id


@task(name='create_duplicate', max_retries=3)
def create_duplicate(account_id, md5):
    """ Create new ticket. """

    return Duplicate.objects.create(account_id=account_id, md5=md5).id


@task(name='new_ticket', max_retries=3)
def new_ticket(id):
    try:
        logger.warning("{} - New".format(id))
        ticket = Ticket.objects.get(id=id)
        ticket_json = TicketInboxSerializer(ticket).data

        # Notify all users
        send_to_client('all', settings.CHANNEL_COMMANDS['NEW_TICKET'], ticket_json)

        queue_groups = get_queue_bygroup(ticket)

        # check if is possible assignment right now
        #users_online = User.objects.filter(status=User.ONLINE).count()
        #ticket_working = Ticket.objects.filter(status=Ticket.WORKING).count()
        users_online = User.objects.filter(status=User.ONLINE, my_groups__id=ticket.group.id).count()
        ticket_working = Ticket.objects.filter(status=Ticket.WORKING, group__in=queue_groups).count()
        ticket_pending = Ticket.objects.filter(status=Ticket.PENDING, group__in=queue_groups).count()
        print("users", users_online)
        print("ticket", ticket_working)

        if (users_online * settings.SIMULTANEOUS_TICKET) > ticket_working:
            serve_ticket.delay(ticket.id)
            logger.warning("{} - Trigger serve_ticket".format(id))

    except Ticket.DoesNotExist:
        logger.error('{} - new_ticket => Ticket not found'.format(id))


@task(name='serve_ticket', max_retries=3)
def serve_ticket(ticket_id=None, user_id=None):
    if ticket_id is None:
        user_groups = get_queue_byuser(user_id)
        ticket = Ticket.fifo_objects.next(user_groups)
        logger.warning("{} - Return ticket".format(ticket))
    else:
        ticket = Ticket.objects.get(id=ticket_id)

    if ticket is None:
        return

    logger.warning("{} - Serve".format(ticket))

    queue_groups = get_queue_bygroup(ticket)

    direct_serve = ticket_id is not None
    users_to_serve = User.objects.filter(
        status=User.ONLINE,
        level__lte=User.SUPERVISOR,
        my_groups__in=queue_groups
    )
    users_to_serve = shuffle_queryset(users_to_serve)
    logger.warning("{} - Users to serve: {}".format(ticket, users_to_serve))

    for user in users_to_serve:
        ticket_working = user.tickets.filter(status=Ticket.WORKING).count()
        available_by_group = user.my_groups.filter(id=ticket.group.id).exists()
        logger.warning("{} - User {}: ticket working {}. available {}".
                       format(ticket, user, ticket_working, available_by_group))

        if ticket_working < settings.SIMULTANEOUS_TICKET and available_by_group:
            try:
                now = datetime.now(timezone.utc)

                with transaction.atomic():
                    sid = transaction.savepoint()

                    ticket.status = Ticket.WORKING
                    ticket.waittime = now - ticket.arrived
                    ticket.last_user = user
                    if direct_serve:
                        ticket.is_served = True
                    ticket.save()

                    # Create Taken
                    obj, taken_created = Taken.objects.get_or_create(
                        status=Ticket.WORKING,
                        ticket=ticket,
                        defaults={
                            'user': user,
                            'start': now
                        }
                    )

                    if taken_created:
                        transaction.savepoint_commit(sid)

                        ticket_json = TicketModelSerializer(ticket).data

                        # Send update to all
                        send_to_client('all', settings.CHANNEL_COMMANDS['UPDATE_TICKET'], ticket_json)

                        # Send ticket to user
                        send_to_client('user_' + str(user.id), settings.CHANNEL_COMMANDS['WORK_TICKET'],
                                       ticket_json)

                        logger.warning("{} - Saved".format(ticket))
                        break
                    else:
                        transaction.savepoint_rollback(sid)
            except Exception as e:
                logger.error('{} - serve_ticket => User {}. Error: {}'.format(ticket, user, str(e)))
                ticket.refresh_from_db()
    # End For
    if ticket.status == Ticket.PENDING and ticket.is_served:
        logger.warning('{} - fifo_objects rollback'.format(ticket))
        Ticket.fifo_objects.rollback(ticket)
    logger.warning('{} - End. status: {}, served: {}'.format(ticket, ticket.status, ticket.is_served))


@task(name='save_ticket', max_retries=3)
def save_ticket(data):
    try:
        ticket = Ticket.objects.get(id=data['id'])
    except Ticket.DoesNotExist:
        logger.error('{} - save_ticket => Ticket not found'.format(data['id']))
        return
    try:
        user = User.objects.get(id=data['user'])
    except User.DoesNotExist:
        logger.error('{} - save_ticket => User not found {}'.format(data['id'], data['user']))
        return
    try:
        message = MessagePrefix.objects.get(id=data['message'])
    except MessagePrefix.DoesNotExist:
        logger.error('{} - save_ticket => MessagePrefix not found {}'.format(data['id'], data['message']))
        return

    if user != ticket.last_user:
        logger.error('{} - save_ticket => one user tries to update another user\'s ticket. {}-{}'.
                     format(data['id'], user, ticket.last_user))
        return

    try:
        taken = Taken.objects.get(ticket=ticket, status=Ticket.WORKING)
    except Taken.DoesNotExist:
        logger.error('{} - save_ticket => Taken not found'.format(data['id']))
    except Taken.MultipleObjectsReturned:
        logger.error('{} - save_ticket => Taken found multi result'.format(data['id']))
        taken = Taken.objects.filter(ticket=ticket, status=Ticket.WORKING).order_by('-id').first()

    now = datetime.now(timezone.utc)

    # Update Ticket
    ticket.status = data['status']
    ticket.last_result = message
    ticket.last_duration = now - taken.start
    ticket.save()

    # Update Taken
    taken.status = data['status']
    taken.end = now
    taken.duration = now - taken.start
    taken.result = message
    taken.save()

    ticket_json = TicketModelSerializer(ticket).data

    # Send update to all
    send_to_client('all', settings.CHANNEL_COMMANDS['UPDATE_TICKET'], ticket_json)

    # Send update to user
    send_to_client('user_' + str(user.id), settings.CHANNEL_COMMANDS['SAVE_TICKET_SUCCESS'], ticket_json)


def send_to_client(group, command, data):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        group,
        {
            'type': 'send_json',
            'message': {'command': command, 'data': data}
        }
    )


def shuffle_queryset(queryset):
    queryset_list = list(queryset)
    random.shuffle(queryset_list)
    return queryset_list


@periodic_task(name='save_agents_availability', run_every=timedelta(minutes=1))
def save_agents_availability():
    tickets_working = Ticket.objects.filter(status=Ticket.WORKING).count()
    users_online = User.objects.filter(status=User.ONLINE).count()

    if users_online != 0:
        UserAvailability.objects.create(
            tickets_working=tickets_working,
            users_online=users_online,
            availability=get_index_user_availability(tickets_working, users_online)
        )


def get_queue_bygroup(ticket):
    """ Return queue's group which contains the given group. """
    queue = Queue.objects.filter(groups__id=ticket.group.id).first()
    logger.warning('{} - Queue selected: {}'.format(ticket, queue))
    return queue.groups.all()

def get_queue_byuser(user_id):
    """ Return id group's array of a user. """
    user = User.objects.get(id=user_id)
    return list(user.my_groups.values_list('id', flat=True))
