""" Class Auth by Icepor. """

# Django
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

# Util
import urllib.request
import json

class AuthIceporBackend(ModelBackend):
    """
    Authenticate against LDAP Icepor by API call
    
    """
    def authenticate(self, request, username=None, password=None):
        """ Valitate username against ICEPOR API.
            If it doesn't exist, the user is created
        """
        UserModel = get_user_model()    
        
        try:
            parm = { 'username': username, 'password': password }
            query_string = urllib.parse.urlencode( parm )   
            data = query_string.encode( "ascii" )       
            response = urllib.request.urlopen(settings.ICEPOR_AUTH, data ).read()
            valid = (json.loads(response))['success']
        except Exception:
            return None

        if valid:
            try:
                user = UserModel.objects.get(username=username)
            except UserModel.DoesNotExist:
                user = UserModel(username=username)
                user.save()
            return user
        return None

    def get_user(self, user_id):
        UserModel = get_user_model()    
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None


