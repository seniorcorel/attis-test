""" Base API Test class. """

# Django
from django.contrib.auth import get_user_model
from django.urls import reverse

# Django REST Framework
from rest_framework.test import APITestCase


class BaseAPITestCase(APITestCase):
    """ Class base to do test with some util functions  """

    def login_user(self, user_data, create=True):
        """ Create and login user to consume API, return user created. """

        if create:
            get_user_model().objects.create_user(**user_data)

        login_data = {
            'username': user_data['username'],
            'password': user_data['password']
        }

        response_user = self.client.post(reverse('users:users-login'), login_data)
        self.token = 'Token {}'.format(response_user.data['access_token'])
        self.client.credentials(HTTP_AUTHORIZATION=self.token)
        return get_user_model().objects.get(id=response_user.data['user']['id'])
