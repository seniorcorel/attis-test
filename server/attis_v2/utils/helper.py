
# Util
import hashlib
from django.contrib.gis.geoip2 import GeoIP2
from django.conf import settings

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_md5_file(fname):
    hash_md5 = hashlib.md5()
    #with open(fname, "rb") as f:
    #    for chunk in iter(lambda: f.read(4096), b""):
    #        hash_md5.update(chunk)
    for chunk in fname.chunks():
        hash_md5.update(chunk)
    return hash_md5.hexdigest()

def get_geo_info(ip):
    try:
        g = GeoIP2()
        geoip = g.city(ip)
    except Exception:
        geoip = {'country_name': 'Home', 'city': 'Home', 'latitude': 0, 'longitude':0}
    return geoip

def format_error(field, message, extra=None):
    result = {field: message}
    if (extra is not None):
        result['extra'] = extra
    return result

def get_index_user_availability(tickets_working, users_online):
    """ Index formule: 100 - ((ticket working * 100) / (users online * simultaneous ticket)). """
    return 100 - ((tickets_working * 100) / (users_online * settings.SIMULTANEOUS_TICKET))
