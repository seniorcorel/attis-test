""" Class Auth by Microsoft """

# Django
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
import requests


class AuthMicrosoftBackend(ModelBackend):
    """Authenticate Microsoft Access Token

    methods:
        authenticate: validates the data provided for the user performing a
        request against microsoft api to retrieve the user account using the
        access token. Response must be OK, so the username exists and the token
        is valid.
            params:
                username: the user is trying to login
                password: access token acquired in frontend
            returns:
                user: obj UserModel

        get_user: gets the user obj from the Model
            params:
                user_id: str to identify the user
            returns:
                user: UserModel
    """

    def authenticate(self, _, username=None, password=None):
        UserModel = get_user_model()

        headers = {
            'Authorization': 'Bearer {}'.format(password),
            'Content-Type': 'application/json'
        }

        url = settings.AZURE_URL+"{}".format(username)

        api_call = requests.get(url=url, headers=headers)
        if api_call.status_code == 200:
            user_name = username.split('@')[0]
            try:
                user = UserModel.objects.get(username=user_name)
            except UserModel.DoesNotExist:
                user = UserModel(username=user_name)
                user.save()
            return user
        return None

    def get_user(self, user_id):
        UserModel = get_user_model()
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None

