"""Main URLs module."""

from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    # Django Admin
    path(settings.ADMIN_URL, admin.site.urls),

    path('api/', include(('attis_v2.api.urls', 'api'), namespace='apis')),
    path('api/', include(('attis_v2.users.urls', 'users'), namespace='users')),
    path('', include(('attis_v2.core.urls', 'core'), namespace='core')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
