import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

export enum TICKET_STATUS {
    PENDING,
    WORKING,
    SUCCESS,
    REJECTED,
    TRANSFERRED,
    AUTOREJECTED,
    TEMP_REJECTED
}

@Injectable()
export class GlobalService {

    private config = null;
    public current_ticket = null;    
    public key = {
        data: 'data',
        id: 'id'
    };
  
    constructor(private _http: HttpClient, private _trans: TranslateService) {
    }

    // CONFIG SECTION 
    load() {
        let data = this.get_data();
        let domain = (data != null) ? data.domain : window['domain']
        return this._http.get(environment.URL_BASE_ASSETS + "config/site.json")
            .toPromise()
            .then((data) => {
                if (data[domain] != null){
                    this.config = data[domain];
                    this.set_lang();
                }else
                    console.error("Domain settings not found")
            });
    }

    public get_config(key: string) {
        if (this.config == null)
            return null;
        return (typeof (this.config[key]) === "undefined") ? null : this.config[key];
    }

    // SITE SECTION
    public set_lang(lang: string=null) {
        lang = (lang==null) ? this.config.lang : lang
        this._trans.setDefaultLang(lang);
    }

    public get_data(){
        return JSON.parse(localStorage.getItem(this.key.data))
    }

    public get_id(){
        return localStorage.getItem(this.key.id)
    }

    public get_domain(){
        let data = this.get_data()
        return (data!=null) ? data.domain : null
    }
}