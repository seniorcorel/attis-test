import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GlobalService } from 'src/app/services/global.service';

@Injectable()
export class ThemeService {

    private default_theme = 'betboo-br';
    private current_theme = null
    constructor(private _http: HttpClient, private _global: GlobalService) {
    }

    set(key = null) {
        if (key == null)
            key = this._global.get_domain()

        return this._http.get(environment.URL_BASE_ASSETS + "config/themes.json")
            .toPromise()
            .then((data) => {
                if (data[key] != null)
                    this.setTheme(data[key], key);
                else {
                    console.log("Theme not found: ", key)
                    this.setTheme(data[this.default_theme], this.default_theme);
                }
            });
    }

    get_current() {
        return this.current_theme
    }

    private setTheme(theme: {}, key) {
        this.current_theme = key;
        Object.keys(theme).forEach(k =>
            document.documentElement.style.setProperty(`--${k}`, theme[k])
        );
    }
}