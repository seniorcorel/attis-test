import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ApiService {

    static GET = 1;
    static POST = 2;
    
    constructor(private _http: HttpClient) {
    }

    private get_url(url:string, pk:string=null){
        return environment.URL_API + url.replace('${pk}', pk)
    }

    private generate_parms<T>(obj: { [key: string]: T } = null): any {
        let params = new HttpParams();
        if (obj != null) {
            Object.keys(obj).forEach(key => {
                if (obj[key] != null)
                    params = params.append(key, obj[key].toString());
            });
        }
        return params
    }

    private generate_forms(obj: { [key: string]: any } = null): any {
        let params = new FormData();
        if (obj != null) {
            Object.keys(obj).forEach(key => {
                if (obj[key] != null){
                    if (obj[key] instanceof File)    
                        params.append(key, <File>obj[key]);
                    else
                        params.append(key, obj[key].toString());
                }
            });
        }
        return params
    }

    public get_ticket(id: string){
        return this._http.get(this.get_url('tickets/${pk}/', id.toString()))
    }

    public get_history(data: any){
        return this._http.get(this.get_url('tickets/'), {params: this.generate_parms(data)})
    }

    public create_ticket(data: any){
        return this._http.post(this.get_url('tickets/'), this.generate_forms(data))
    }
}