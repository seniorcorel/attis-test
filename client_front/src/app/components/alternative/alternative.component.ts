import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GlobalService } from 'src/app/services/global.service';
import { Router } from '@angular/router';
import { slideInOutAnimation } from 'src/app/animations/slide'

@Component({
  selector: 'app-alternative',
  templateUrl: './alternative.component.html',
  styles: [],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})
export class AlternativeComponent implements OnInit {

  current_ticket = {
    id:null,
    status:null,
    last_result: null,
    is_autoreject: false
  }
  constructor(private _api: ApiService, private _global: GlobalService, private _router: Router) {
    this.current_ticket = this._global.current_ticket;

    if (this._global.current_ticket == null)
      this._router.navigate(['/created'])
  }

  ngOnInit() {
  }

}
