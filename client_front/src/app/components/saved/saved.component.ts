import { Component, OnInit } from '@angular/core';
import { GlobalService, TICKET_STATUS } from 'src/app/services/global.service';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { slideInOutAnimation } from 'src/app/animations/slide'

@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styles: [],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})
export class SavedComponent implements OnInit {

  current_ticket_default = {
    id: null,
    status: null,
    last_result: null
  }
  current_ticket = {
    id: null,
    status: null,
    last_result: null
  }
  interval = null;
  asking = true
  ticket_status = TICKET_STATUS;
  have_working = false
  show_delay_message = false
    
  constructor(private _global: GlobalService, private _api: ApiService, private _router: Router) {
    let id = localStorage.getItem(this._global.key.id)
    
    if (this._global.current_ticket == null) {
      if (id != null) {
        this.get_ticket()
        let time_interval = this._global.get_config('wait_get_ticket') || 1000;
        let time_kill_interval = this._global.get_config('kill_wait_get_ticket') || 600000;

        // Set Interval get status
        this.interval = setInterval(() => { this.get_ticket() }, time_interval);

        // Set kill interval
        setTimeout(()=>{ this.asking = false, clearInterval(this.interval)}, time_kill_interval)

        // To show message after check One-ticket-at-a-time
        if ('have_working' in window.history.state )
            this.have_working = true

        // Set Timeout to show delay message in 30 min
        setTimeout(() => { this.show_delay_message=true }, 30*60000);

      } else
        this._router.navigate(['/created'])
    }else
      this.current_ticket = this._global.current_ticket
  }

  ngOnInit() {
  }

  get_ticket(){
    let id = localStorage.getItem(this._global.key.id)
    this._api.get_ticket(id).subscribe(
      (res:any) => {
        this._global.current_ticket = this.current_ticket = res
        
        if (this._global.current_ticket.status >= TICKET_STATUS.SUCCESS){
          this._global.current_ticket = null
          localStorage.removeItem(this._global.key.id)
          clearInterval(this.interval)
        }        
      },
      err => {
        console.log(err)
      }
    )
  }

  reload(){
    location.reload();
  }

}
