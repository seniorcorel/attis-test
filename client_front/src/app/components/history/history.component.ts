import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GlobalService, TICKET_STATUS } from 'src/app/services/global.service';
import { Router } from '@angular/router';
import { slideInOutAnimation } from 'src/app/animations/slide'

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styles: [],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})
export class HistoryComponent implements OnInit {

  list = [];
  ticket_status = TICKET_STATUS;
  
  constructor(private _api: ApiService, private _global: GlobalService, private _router: Router) {
    let data = <any>this._global.get_data()
    console.log(data)

    if (data.account_id == null){
      this._router.navigate(['/created'])
      return;
    }

    this._api.get_history({account_id: data.account_id}).subscribe(
      (res:any) => {
        console.log(res);
        this.list = res.results;
      },
      err => {
        console.log(err);
      }
    )


    
  }

  ngOnInit() {
  }

}
