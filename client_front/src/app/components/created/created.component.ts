import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from 'src/app/services/api.service';
import { GlobalService, TICKET_STATUS } from 'src/app/services/global.service';
import { ThemeService } from 'src/app/services/theme.service';
import { slideInOutAnimation } from 'src/app/animations/slide'

@Component({
  selector: 'app-created',
  templateUrl: './created.component.html',
  styles: [],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})
export class CreatedComponent implements OnInit {

  data = {
    account_id: null,
    account_name: null,
    domain: null,
    group: null
  }
  aux = {
    error_message: null,
    url_read_more: '',
    loading: false
  }


  constructor(
  private _route: ActivatedRoute,
  private _translate: TranslateService,
  private _api: ApiService,
  private _router: Router,
  private _global: GlobalService,
  private _theme: ThemeService) { }

  ngOnInit() {
    this._route.queryParams.subscribe(parms => {
      this.data.account_id = parms['account_id'];
      this.data.account_name = parms['account_name'];
      this.data.domain = parms['domain'];
      this.data.group = parms['group'];
      
      // Set Lang
      if (parms['lang'] != null)
        this._global.set_lang(parms['lang'])

      // Have ticket in progress (local)
      if (localStorage.getItem(this._global.key.id) != null)
        this._router.navigate(['/saved'])



      // Check mandatory fields
      for (var key in this.data) {
        if (this.data[key] == null || this.data[key] == "") {
          let data = JSON.parse(localStorage.getItem(this._global.key.data))
          if (data == null)
            return this.show_message('missing_required_fields')
          else
            this.data = data
        }
      }

      // Save mandatory fields
      localStorage.setItem(this._global.key.data, JSON.stringify(this.data));

      // Set Theme
      this._theme.set(this.data.domain)
    });


    // Have ticket in progress (api)
  this._api.get_history({account_id: this.data.account_id}).subscribe(
      (res:any) => {
        let working = res.results.filter(item => item.status <= TICKET_STATUS.WORKING );
        if (working.length > 0)
            this.set_and_send_to_saved(working[0]['id'])
      },
      err => {
        console.log(err);
      }
    )

    this.aux.url_read_more = this._global.get_config('url_read_more')
  }

  show_message(key: string) {
    this._translate.get(key).subscribe(res => {
      this.aux.error_message = res
    });
  }

  create_ticket(fileInput: any) {
    this.aux.loading = true
    let data = this.data;
    data['file'] = <File>fileInput.target.files[0];


    this._api.create_ticket(data).subscribe(

      // SUCCESS
      res => {
        console.log(res);
        this._global.current_ticket = null
        localStorage.setItem(this._global.key.id, res['id'])
        this._router.navigate(['/saved'])
      },

      // ERROR
      err => {
        console.log(err)
        if (err.error['file'] != null) {

          // DUPLICATE
          if (err.error['file'].some(e => e == "File duplicate")) {
            this._global.current_ticket = err.error['extra'];
            this._router.navigate(['/alternative'])
          }

          // AUTOREJECT
          if (err.error['file'].some(e => e == "Customer in autoreject")) {
            this._global.current_ticket = {
              status: TICKET_STATUS.REJECTED,
              last_result: { body: err.error['extra'] },
              is_autoreject: true
            };
            this._router.navigate(['/alternative'])
          }

          // Extension not allowed
          if (err.error['file'].some(e => e.includes("La extensión de fichero"))){
            this.show_message('Extension not allowed')
            this.aux.loading = false
           }

          // Max file size
          if (err.error['file'].some(e => e == "The maximum file size that can be uploaded is 3MB")){
            this.show_message('The maximum file size that can be uploaded is 3MB')
            this.aux.loading = false
          }

          // Check One-ticket-at-a-time
          if (err.error['file'].some(e => e == "Already ticket on working")) {
            console.log(err.error['extra'].id)
            this.set_and_send_to_saved(err.error['extra'].id)
          }
        }
      }
    )
  }

  set_and_send_to_saved(id){
    this._global.current_ticket = null
    localStorage.setItem(this._global.key.id, id)
    this._router.navigate(['/saved'], {state: {have_working:true}})
  }

}
