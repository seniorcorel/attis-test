import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreatedComponent } from './created.component';

describe('CreatedComponent', () => {
  let component: CreatedComponent;
  let fixture: ComponentFixture<CreatedComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
