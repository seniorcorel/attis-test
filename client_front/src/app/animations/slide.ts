import { trigger, state, animate, transition, style } from '@angular/animations';

export const slideInOutAnimation =
    trigger('slideInOutAnimation', [
        state('*', style({
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        })),

        // route 'enter' transition
        transition(':enter', [
            style({
                right: '-50%',
                opacity: '0'
            }),
            animate('.3s ease-in-out', style({
                right: 0,
                opacity: '1'
            }))
        ]),

        // route 'leave' transition
        transition(':leave', [
            animate('.3s ease-in-out', style({
                right: '50%',
                opacity: '0'
            }))
        ])
    ]);