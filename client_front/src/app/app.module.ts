// Angualr
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient} from'@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Third
import { NgbTooltipModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';


// Own
import { environment } from'../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreatedComponent } from './components/created/created.component';
import { SavedComponent } from './components/saved/saved.component';
import { AlternativeComponent } from './components/alternative/alternative.component';
import { HistoryComponent } from './components/history/history.component';
import { ApiService } from './services/api.service'
import { GlobalService } from './services/global.service'
import { ThemeService } from './services/theme.service'
import { DateAgoPipe } from './pipes/date-ago.pipe'

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, environment.URL_BASE_ASSETS + 'i18n/', '.json');
}

export function configSeviceFactory(provider: GlobalService) {
  return () => provider.load();
}

@NgModule({
  declarations: [
    AppComponent,
    CreatedComponent,
    SavedComponent,
    AlternativeComponent,
    HistoryComponent,
    DateAgoPipe
  ],
  imports: [
    NgbTooltipModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({ 
      loader: { provide: TranslateLoader, useFactory: HttpLoaderFactory, deps: [HttpClient] },
      }),
    NgbModule,
  ],
  providers: [
    ApiService, 
    GlobalService,
    ThemeService,
    { provide: APP_INITIALIZER, useFactory: configSeviceFactory, deps: [GlobalService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
