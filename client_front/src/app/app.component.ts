import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from './services/global.service';
import { ThemeService } from './services/theme.service';
import { Router, ActivatedRoute } from '@angular/router';
import { fadeOutAnimation } from './animations/fade'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [fadeOutAnimation]
})
export class AppComponent {

    loading=true

    constructor(
    private _trans: TranslateService,
    private _global: GlobalService,
    private _theme: ThemeService,
    private _route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this._theme.set().then(
            () => this.loading = false
        )
    }
}
