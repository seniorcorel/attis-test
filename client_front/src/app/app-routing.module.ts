import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatedComponent } from './components/created/created.component';
import { AlternativeComponent } from './components/alternative/alternative.component';
import { SavedComponent } from './components/saved/saved.component';
import { HistoryComponent } from './components/history/history.component';

const routes: Routes = [
  {path: 'created', component: CreatedComponent},
  {path: 'alternative', component: AlternativeComponent},
  {path: 'saved', component: SavedComponent},
  {path: 'history', component: HistoryComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'created'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
