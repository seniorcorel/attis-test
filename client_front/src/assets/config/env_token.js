(function(window) {
  window["env"] = window["env"] || {};

  window["env"]["URL_BASE_ASSETS"] = "${URL_BASE_ASSETS}";
  window["env"]["URL_API"] = "${URL_API}";
})(this);