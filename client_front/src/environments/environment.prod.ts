
export const environment = {
  production: true,
  URL_BASE_ASSETS: window["env"]["URL_BASE_ASSETS"],
  URL_API: window["env"]["URL_API"]
};
