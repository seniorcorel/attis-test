
export const environment = {
  production: false,
  URL_BASE_ASSETS: 'http://localhost:4200/assets/',
  URL_API: 'http://localhost:8000/api/'
};
