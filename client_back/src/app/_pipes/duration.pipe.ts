import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'duration' })
export class DurationPipe implements PipeTransform {
    /* Receive milliseconds and return duration un format mm:ss */
    transform(value: number): string {
       //const minutes: number = Math.floor(value / 60);
       //return minutes.toString().padStart(2, '0') + ':' + (value - minutes * 60).toString().padStart(2, '0');

        var oneSecond = 1000;
        var oneMinute = oneSecond * 60;
        var oneHour = oneMinute * 60;
        var oneDay = oneHour * 24;

        var seconds = Math.floor((value % oneMinute) / oneSecond);
        var minutes = Math.floor((value % oneHour) / oneMinute);
        var hours = Math.floor((value % oneDay) / oneHour);

        return hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0') + ':' + seconds.toString().padStart(2, '0')
    }
}
