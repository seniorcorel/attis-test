import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'diff' })
export class DiffPipe implements PipeTransform {
    /* Return difference (in seconds) between a date and now */
    transform(value: string): Number {
        let from = new Date(value)
        let now = new Date()
        var dif = now.getTime() - from.getTime();
        return Math.abs(dif / 1000);
    }
}
