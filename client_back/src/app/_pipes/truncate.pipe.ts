import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'truncate' })
export class TruncatePipe implements PipeTransform {
    /* Return a substring with three dots */
    transform(value: any, limit: Number): string {
        const trail = '...';
        return value.length > limit ? value.substring(0, limit) + trail : value;
   }
}