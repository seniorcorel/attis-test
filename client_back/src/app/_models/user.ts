export class User {
    id: number;
    username: string;
    password: string;
    level: number;
    status: number;
    has_autoassign: boolean;
    token?: string;    
}