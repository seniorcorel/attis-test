export enum TICKET_COMMANDS {
  GET_INBOX = 'get_inbox',
  NEW_TICKET = 'new_ticket',
  UPDATE_TICKET = 'update_ticket',
  WORK_TICKET = 'work_ticket',
  SAVE_TICKET = 'save_ticket',
  SAVE_TICKET_SUCCESS = 'save_ticket_success',
  SERVE = 'serve',
  GET_MESSAGEPREFIX = 'get_messageprefix',
  GET_INTERNAL = 'get_internal',
  TRANSFER_TICKET = 'transfer_ticket',
  GET_USERS_ONLINE = 'get_users_online',
  GET_TICKET = 'get_ticket',
  FORCE_LOGOUT = 'force_logout',
  TOAST = 'toast',
  GET_TICKET_BY_STATUS = 'get_ticket_by_status'
}

export enum TICKET_STATUS {
  PENDING = 0,
  WORKING = 1,
  SUCCESS = 2,
  REJECTED = 3,
  TRANSFERRED = 4,
  AUTOREJECTED = 5,
  TEMP_REJECTED = 6
}

export enum USER_STATUS {
  ONLINE = 0,
  BACK_5 = 1,
  BREAK = 2,
  PRELOGOFF = 3,
  OFFLINE = 4,
}
export namespace USER_STATUS {
  export function values() {
    return Object.keys(USER_STATUS).filter(
      (type) => isNaN(<any>type) && type !== 'values'
    );
  }
}

export enum USER_LEVELS {
  STANDARD = 0,
  SUPERVISOR = 1,
  ADMIN = 2,
}