import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from './_services/authentication.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  show_nav = true

  constructor(
    private _trans: TranslateService,
    private _activated_route: ActivatedRoute,
    private _auth: AuthenticationService,
    private _router: Router) {
        this._trans.setDefaultLang('es');
  }

  ngOnInit() {
    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd)
        this.show_nav = this._activated_route.firstChild.snapshot.data.show_nav !== false;
    });

    this._auth.get_status_from_server();
  }
}
