// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Third
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgSelectModule } from '@ng-select/ng-select';
import { ChartsModule } from 'ng2-charts';
import { CdTimerModule } from 'angular-cd-timer';
import { ImageViewerModule } from 'ng2-image-viewer';
import { Subscription } from 'rxjs';
import { MsalModule,
  //MsalInterceptor,
  MSAL_CONFIG,
  MSAL_CONFIG_ANGULAR,
  MsalService,
  MsalAngularConfiguration } from '@azure/msal-angular';
import { Configuration } from 'msal';

// Own
import { environment } from '@env/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from '@app/components/shared/nav/nav.component';
import { DashboardComponent } from '@app/components/dashboard/dashboard.component';
import { GroupsComponent } from '@app/components/groups/groups.component';
import { MessagesComponent } from '@app/components/messages/messages.component';
import { InternalsComponent } from '@app/components/internals/internals.component';
import { GroupAssignmentComponent } from '@app/components/group-assignment/group-assignment.component';
import { AutorejectsComponent } from '@app/components/autorejects/autorejects.component';
import { ReportsComponent } from '@app/components/reports/reports.component';
import { AgentsComponent } from '@app/components/agents/agents.component';
import { InboxComponent } from '@app/components/inbox/inbox.component';
import { LoginComponent } from '@app/components/login/login.component'
import { ToastComponent } from '@app/components/shared/toast/toast.component'
import {  RestService, 
          AuthenticationService, 
          MessageprefixesService, 
          MessageinternalsService,
          GroupsService,
          AutorejectsService,
          UsersService,
          AssignmentsService,
          WebsocketService,
          TicketsService,
          ExternalService,
          ReportsService,
          PushNotificationsService } from '@app/_services'
import { JwtInterceptor, ErrorInterceptor } from '@app/_helpers';
import { DiffPipe,
         DurationPipe,
         TruncatePipe } from '@app/_pipes'


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, environment.URL_BASE_ASSETS + 'i18n/', '.json');
}

//export const  isIE = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);

export function MSALConfigFactory(): Configuration {
return {
    auth: {
        clientId: environment.AZURE_CLIENT_ID,
        postLogoutRedirectUri: environment.URL_AZURE_POSTLOGOUT,
        redirectUri: environment.URL_AZURE_REDIRECT,
        authority: environment.AZURE_TENANTID
    },
    cache: {
      cacheLocation: "localStorage",
      //storeAuthStateInCookie: isIE, // set to true for IE 11
    },
  };
}

export function MSALAngularConfigFactory(): MsalAngularConfiguration {
    return {
        popUp: false,
        protectedResourceMap: [['https://graph.microsoft.com/v1.0/me', ['user.read']]],
    };
}

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    DashboardComponent,
    GroupsComponent,
    MessagesComponent,
    InternalsComponent,
    GroupAssignmentComponent,
    AutorejectsComponent,
    ReportsComponent,
    AgentsComponent,
    InboxComponent,
    LoginComponent,
    ToastComponent,
    DiffPipe,
    DurationPipe,
    TruncatePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ChartsModule,
    HttpClientModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: { provide: TranslateLoader, useFactory: HttpLoaderFactory, deps: [HttpClient] },
    }),
    SweetAlert2Module.forRoot(),
    NgSelectModule,
    FormsModule,
    CdTimerModule,
    ImageViewerModule,
    MsalModule.forRoot({
            auth: {
                clientId: environment.AZURE_CLIENT_ID,
                postLogoutRedirectUri: environment.URL_AZURE_POSTLOGOUT,
                redirectUri: environment.URL_AZURE_REDIRECT,
                authority: environment.AZURE_TENANTID
            },
            cache: {
                cacheLocation: 'localStorage',
                //storeAuthStateInCookie: isIE, // set to true for IE 11
            },
        },
        {
            protectedResourceMap: [
                ['https://graph.microsoft.com/v1.0/me', ['user.read']]
            ]
        })
  ],
  providers: [
    DatePipe,
    RestService,
    AuthenticationService,
    MessageprefixesService,
    MessageinternalsService,
    GroupsService,
    AutorejectsService,
    UsersService,
    AssignmentsService,
    WebsocketService,
    TicketsService,
    ExternalService,
    ReportsService,
    PushNotificationsService,

    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    //{ provide: HTTP_INTERCEPTORS, useClass: MsalInterceptor, multi: true },
    { provide: MSAL_CONFIG, useFactory: MSALConfigFactory },
    { provide: MSAL_CONFIG_ANGULAR, useFactory: MSALAngularConfigFactory },
    MsalService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
