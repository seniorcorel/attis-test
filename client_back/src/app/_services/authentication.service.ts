import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@env/environment';
import { User } from '@app/_models';
import { StatusService } from './status.service';

import { MsalService } from '@azure/msal-angular';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private localstorage_key = 'current_user';

    constructor(private http: HttpClient, private _status: StatusService, public msalService: MsalService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(this.localstorage_key)));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    /*
    Refactor Office 365

    Join two functions as one even if the first is no longer in use.
    Params:
        username: string (user to authenticates)
        password: string (password or token)
        enpoint: string (login or token)

        returns data from Django API Users
    */
    login_or_token(username: string, password: string, endpoint: string) {
        return this.http.post<any>(`${environment.URL_API}users/`+endpoint+`/`, { username, password })
            .pipe(map(user => {
                let data = user.user
                data['token'] = user.access_token

                localStorage.setItem(this.localstorage_key, JSON.stringify(data));
                this.currentUserSubject.next(data);

                this._status.set_status(user.user.status, false)
                return data;
            }));
    }

    // Refactor  Office 365: get logged account in the browser
    get_microsoft_account(){
        return this.msalService.getAccount()
    }

    // Refactor  Office 365: Logout from Office 365
    logout() {
        // remove all from localStorage
        this.currentUserSubject.next(null);
        localStorage.clear();
        this.msalService.logout();
    }

    public get_status_from_server() {
        const user = this.currentUserValue
        console.log(user)
        if (user != null) {
            this.http.get(environment.URL_API + `users/${user.id}/`)
                .subscribe(
                    (data: any) => this._status.set_status(data.status, false)
                )
        }
    }
}