import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class UsersService extends RestService {

  constructor(_http: HttpClient) {
    super(_http);
    this._url_base = environment.URL_API;
    this._resource = 'users';
  }

  public users_now() {
    /* Get users list */
    return this._http.get(this.get_api_url(`${this._resource}/now`))
  }

  public force_logout(user_id) {
    /* Get users list */
    return this._http.post(this.get_api_url(`${this._resource}/force_logout`), {'target_user': user_id})
  }

}
