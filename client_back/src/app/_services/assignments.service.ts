import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class AssignmentsService extends RestService {

  constructor(_http: HttpClient) {
    super(_http);
    this._url_base = environment.URL_API;
    this._resource = 'assignments';
  }

  public byuser(values: any) {
    /* Save and assign groups to user */
    return this._http.post(this.get_api_url(this._resource) + 'assign_by_user/', values)
  }

  public get_mygroups(id: Number) {
    /* Get groups list by user */
    let params = this.generate_parms({ object: 'user' })
    return this._http.get(this.get_api_url(`${this._resource}/${id}`), { params: params })
  }

  public get_myusers(id: Number) {
    /* Get users list by group */
    let params = this.generate_parms({ object: 'group' })
    return this._http.get(this.get_api_url(`${this._resource}/${id}`), { params: params })
  }

}
