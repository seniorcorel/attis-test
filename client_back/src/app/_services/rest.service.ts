import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject, throwError, generate, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Page } from '@app/_models';


@Injectable()
export class RestService {

    protected _url_base: string
    protected _resource: string
    
    constructor(protected _http: HttpClient) {
    }

    protected get_api_url(url: string) {
        /* Get endpoint formated */
        if (this._url_base == null)
            throw new Error('URL BASE not found');
       return this._url_base + (url.endsWith('/') ? url : url + '/');
    }

    
    protected generate_parms<T>(obj: { [key: string]: T } = null): any {
        /* Generate GET params in object */
        let params = new HttpParams();
        if (obj != null) {
            Object.keys(obj).forEach(key => {
                if (obj[key] != null)
                    params = params.append(key, obj[key].toString());
            });
        }
        return params
    }

    /*private generate_forms(obj: { [key: string]: any } = null): any {
        let params = new FormData();
        if (obj != null) {
            Object.keys(obj).forEach(key => {
                if (obj[key] != null){
                    if (obj[key] instanceof File)    
                        params.append(key, <File>obj[key]);
                    else
                        params.append(key, obj[key].toString());
                }
            });
        }
        return params
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };*/

    public get() {
        /* Call endpoint to get list without pagination */
        let params = this.generate_parms({no_page:true})
        return this._http.get(this.get_api_url(this._resource), {params: params})
    }

    public get_paginated<T>(urlOrFilter?: string | object): Observable<Page<T>> {
        /* Call endpoint to get list paginated */
        let params = new HttpParams();
        let url = this.get_api_url(this._resource);
      
        if (typeof urlOrFilter === 'string') {
          // we were given a page URL, use it
          url += urlOrFilter;
        } else if (typeof urlOrFilter === 'object') {
          // we were given filtering criteria, build the query string
          Object.keys(urlOrFilter).sort().forEach(key => {
            const value = urlOrFilter[key];
            if (value !== null) {
              params = params.set(key, value.toString());
            }
          });
        }
      
        return this._http.get<Page<T>>(url, {
            params: params
        });
      }

    public get_one(id: Number) {
        /* Call endpoint to get one item */
        return this._http.get(this.get_api_url(`${this._resource}/${id}`))
    }

    public post(data: any) {
        /* Call endpoint to save item */
        return this._http.post(this.get_api_url(this._resource), data)
            /*.pipe(
                retry(2),
                catchError(this.handleError)
            )*/
    }

    public put(id: Number, data: any) {
        /* Call endpoint to edit item */
        return this._http.put(this.get_api_url(`${this._resource}/${id}`), data)
    }

    public delete(id: Number) {
        /* Call endpoint to delete item */
        return this._http.delete(this.get_api_url(`${this._resource}/${id}`))
    }

}