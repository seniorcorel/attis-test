import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
    toasts: any[] = [];

    show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
        /* Add toast to Toast stack */
        this.toasts.push({ textOrTpl, ...options });
    }

    remove(toast) {
        /* Remove spesific toast */
        this.toasts = this.toasts.filter(t => t !== toast);
    }

    success(text, title=null){
        /* Show Success toast*/
        this.show(text, { classname: 'bg-success text-light', title:title })
    }

    error(text, title=null){
        /* Show Error toast*/
        this.show(text, { classname: 'bg-danger text-light', title:title })
    }
}