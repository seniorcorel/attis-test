import { Injectable } from "@angular/core";
import * as Rx from "rxjs/Rx";
import { Observable, Subject, Observer, BehaviorSubject } from "rxjs";

@Injectable()
export class WebsocketService {
  constructor() {}

  private subject: Subject<MessageEvent>;
  private is_open_base = new BehaviorSubject<boolean>(false);

  public is_open(){
    return this.is_open_base
  }

  public connect(url): Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);      
      console.log("Successfully connected: " + url);
    }
    return this.subject;
  }

  private create(url): Subject<MessageEvent> {
    let ws = new WebSocket(url);
    ws.onopen = () => this.is_open_base.next(true);

    let observable = Observable.create((obs: Observer<MessageEvent>) => {      
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      },
      error: (data: Object) => {
        console.log("Error connection: ", data);
      }
    };
    return Subject.create(observer, observable);
  }
}