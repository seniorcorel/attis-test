import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class ReportsService extends RestService {

  constructor(_http: HttpClient) {
    super(_http);
    this._url_base = environment.URL_API;
    this._resource = 'reports';
  }

  public line_stats() {
    /* Get line stats */
    return this._http.get(this.get_api_url(`${this._resource}/line_stats`))
  }

  public times_stats() {
    /* Get times_stats */
    return this._http.get(this.get_api_url(`${this._resource}/times_stats`))
  }

  public users_stats() {
    /* Get users_stats */
    return this._http.get(this.get_api_url(`${this._resource}/users_stats`))
  }

  public tickets_stats() {
    /* Get tickets_stats */
    return this._http.get(this.get_api_url(`${this._resource}/tickets_stats`))
  }

}
