import { Injectable, TemplateRef } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { USER_STATUS } from '@app/_models';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment'
import { ToastService } from './toast.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable({ providedIn: 'root' })
export class StatusService {
    private status = new BehaviorSubject<any>(USER_STATUS.BACK_5);

    constructor(private _http: HttpClient, private _toast: ToastService, private _trans: TranslateService) { }

    private get_http(status){
        return this._http.post(environment.URL_API + 'users/change_status/', { status: status })
    }

    public set_status(status, to_server = true) {
        if (to_server) {
            this.get_http(status)
                .subscribe(
                    data => this.status.next(status),
                    err => {
                        this._toast.error(this._trans.instant(err), this._trans.instant('Error changing status'));
                        this.status.next(this.status.value)
                    }
                )
        }else
            this.status.next(status)
    }

    public get_status() {
        return this.status
    }

    public logout(){
        return this.get_http(USER_STATUS.OFFLINE)
    }

}