import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ApiService {
    
    constructor(private _http: HttpClient) {
    }

    private get_api_url(url:string){
        return environment.URL_API + (url.endsWith('/') ? url : url + '/')
    }

    private get_headers(){
        return new HttpHeaders()
            .set("Authorization", "Token " + "0c9d5a32767d3e1a600acb6bc6f11eaa19658dff");
    }

    private generate_parms<T>(obj: { [key: string]: T } = null): any {
        let params = new HttpParams();
        if (obj != null) {
            Object.keys(obj).forEach(key => {
                if (obj[key] != null)
                    params = params.append(key, obj[key].toString());
            });
        }
        return params
    }

    private generate_forms(obj: { [key: string]: any } = null): any {
        let params = new FormData();
        if (obj != null) {
            Object.keys(obj).forEach(key => {
                if (obj[key] != null){
                    if (obj[key] instanceof File)    
                        params.append(key, <File>obj[key]);
                    else
                        params.append(key, obj[key].toString());
                }
            });
        }
        return params
    }

    // MESSAGE PREFIX ---------------------------------------------------------*
    public messageprefix_getall(){
        return this._http.get(this.get_api_url('messageprefixes'), {headers: this.get_headers()})
    }

    public messageprefix_get(id: Number){
        return this._http.get(this.get_api_url(`messageprefixes/${id}`))
    }

    public messageprefix_post(data: any){
        return this._http.post(this.get_api_url('messageprefixes'), data)
    }

    public messageprefix_putch(data: any){
        return this._http.put(this.get_api_url('messageprefixes'), data)
    }

    public messageprefix_delete(id: Number){
        return this._http.delete(this.get_api_url(`messageprefixes/${id}`))
    }
}