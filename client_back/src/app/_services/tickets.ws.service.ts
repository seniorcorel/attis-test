import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
import { map } from 'rxjs/operators';
import { WebsocketService } from "./websocket.service";
import { environment } from '@env/environment'
import { AuthenticationService } from './authentication.service';
import { WebsocketModel } from '@app/_models';

@Injectable()
export class TicketsService {
  public hub: Subject<any>;
  
  constructor(private _ws: WebsocketService, _auth: AuthenticationService) {
    let token = '?token=' + _auth.currentUserValue.token
    this.hub = <Subject<any>>_ws
      .connect(environment.URL_WS + token)
      .pipe(
        map((response: MessageEvent): any => {
            return JSON.parse(response.data);
        })
    );
  }

  public send(command, obj){
    this.hub.next(new WebsocketModel(command, obj));
  }

  public is_open(){
    return this._ws.is_open()
  }
}