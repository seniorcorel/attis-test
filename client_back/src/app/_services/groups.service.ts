import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class GroupsService extends RestService {

  constructor(_http: HttpClient) {
    super(_http);
    this._url_base = environment.URL_API;
    this._resource = 'groups';
  }

}
