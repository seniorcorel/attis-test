import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class ExternalService{

  constructor(private _http: HttpClient) {
  }

  get_cpfinfo(accountid){
    return this._http.get(environment.URL_SYNA_API + accountid)
  }

}
