/* The Error Interceptor intercepts http responses from the api to check 
if there were any errors. If there is a 401 Unauthorized response the user 
is automatically logged out of the application */

import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '@app/_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError(err => {
                if (err.status === 401) {
                    // auto logout if 401 response returned from api
                    this.authenticationService.logout();
                    //location.reload(true);
                }

                if (!err.error){
                    err.error = err
                }

                let error = ''
                let keys_without_details = ['non_field_errors', 'detail']
                for (const [key, value] of Object.entries(err.error)) {
                    if (keys_without_details.includes(key))
                        error = (value instanceof Array) ? value[0] : value
                    else{
                        let _key = key.charAt(0).toUpperCase() + key.slice(1)
                        error = `${_key}: ${value}`
                    }
                    break
                }

                //const error = err.error.message || err.statusText;
                return throwError(error);
            }))
    }
}