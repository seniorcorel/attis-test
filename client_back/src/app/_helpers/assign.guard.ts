/* The assign guard is an angular route guard that's used to prevent 
unauthenticated users from accessing restricted routes depend on user's auto assign value */

import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService, ToastService } from '@app/_services';
import { USER_LEVELS } from '@app/_models'

@Injectable({ providedIn: 'root' })
export class AssignGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private toast: ToastService,
        private tran: TranslateService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser.has_autoassign || currentUser.has_autoassign==false && currentUser.level >= USER_LEVELS.SUPERVISOR ) {
            // User have permission so return true
            return true;
        }

        this.toast.error(this.tran.instant('You do not have permission for this action'), this.tran.instant('Permission error'))
        this.router.navigate(['/']);
        return false;
    }
}