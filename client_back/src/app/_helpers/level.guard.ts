/* The level guard is an angular route guard that's used to prevent 
unauthenticated users from accessing restricted routes depend on user's level */

import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService, ToastService } from '@app/_services';
import { USER_LEVELS } from '@app/_models'


@Injectable({ providedIn: 'root' })
export class LevelGuard implements CanActivate {
    LEVEL: number = USER_LEVELS.STANDARD

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private toast: ToastService,
        private tran: TranslateService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser.level >= this.LEVEL) {
            // Rigth level so return true
            return true;
        }

        // Wrong level so redirect to index page
        this.toast.error(this.tran.instant('You do not have permission for this action'), this.tran.instant('Permission error'))
        this.router.navigate(['/']);
        return false;
    }
}

@Injectable({ providedIn: 'root' })
export class IsStandardLevel extends LevelGuard {
    LEVEL = USER_LEVELS.STANDARD
}

@Injectable({ providedIn: 'root' })
export class IsSupervisorLevel extends LevelGuard {
    LEVEL = USER_LEVELS.SUPERVISOR
}

@Injectable({ providedIn: 'root' })
export class IsAdminLevel extends LevelGuard {
    LEVEL = USER_LEVELS.ADMIN
}