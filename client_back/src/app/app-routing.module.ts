// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Own
import { AgentsComponent } from '@app/components/agents/agents.component';
import { AutorejectsComponent } from '@app/components/autorejects/autorejects.component';
import { DashboardComponent } from '@app/components/dashboard/dashboard.component';
import { GroupAssignmentComponent } from '@app/components/group-assignment/group-assignment.component';
import { GroupsComponent } from '@app/components/groups/groups.component';
import { InboxComponent } from '@app/components/inbox/inbox.component';
import { MessagesComponent } from '@app/components/messages/messages.component';
import { InternalsComponent } from '@app/components/internals/internals.component';
import { ReportsComponent } from '@app/components/reports/reports.component';
import { LoginComponent } from '@app/components/login/login.component';
import { AuthGuard, IsSupervisorLevel, IsAdminLevel, IsStandardLevel, AssignGuard } from '@app/_helpers'


const routes: Routes = [
  // Public
  { path: 'login', component: LoginComponent, data: { show_nav: false } },

  // Protected
  { path: 'messages', component: MessagesComponent, canActivate: [AuthGuard, IsSupervisorLevel] },
  { path: 'internals', component: InternalsComponent, canActivate: [AuthGuard, IsSupervisorLevel] },
  { path: 'groups', component: GroupsComponent, canActivate: [AuthGuard, IsAdminLevel]  },
  { path: 'autorejects', component: AutorejectsComponent, canActivate: [AuthGuard, IsSupervisorLevel]  },
  { path: 'assignment', component: GroupAssignmentComponent, canActivate: [AuthGuard, IsStandardLevel, AssignGuard]  },
  { path: 'agents', component: AgentsComponent, canActivate: [AuthGuard, IsSupervisorLevel]  },
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard, IsSupervisorLevel]  },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard, IsSupervisorLevel]  },
  { path: '', component: InboxComponent, canActivate: [AuthGuard] },
  
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
