import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styles: []
})
export class ReportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  barChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
  };
  barChartLabels: Label[] = ['antonela.geymonat', 'eduardo.bentancur', 'facundo.rebuffo', 'itamara.santos', 'jarcia', 'jmartell', 'jonathan', 'jraspino', 'lvitello', 'manuela.suarez', 'maria.rodo', 'marianela.fernandez', 'martin.lopez'];
  barChartType = 'bar';
  barChartLegend = false;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [2302, 1630, 1317, 1457, 630, 1829, 985, 562, 2250, 1150, 2690, 1254, 1785], label: 'agents' }
  ];
  barChartColors: Color[] = [
    {
      backgroundColor: '#01A66E',
    },
  ];

}
