import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { TicketsService, ToastService } from '@app/_services';
import { ReportsService } from '@app/_services/reports.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment'
import { USER_STATUS, TICKET_STATUS } from '@app/_models'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  env = environment
  loading = {
    line_stats: true,
    times_stats: true,
    users_stats: true,
    tickets_stats: true,
  }

  data = {
    line_stats: null,
    times_stats: null,
    users_stats: null,
    tickets_stats: null,
  }

  constructor(
    private _report:ReportsService, 
    private _toast:ToastService,
    private _trans: TranslateService) { }

  ngOnInit() {

    // Line stats
    this.loading.line_stats = true
    this._report.line_stats().subscribe(
        data => {
            this.data.line_stats = data
        },
        err => this.callback_err("line_stats"),
        () => this.loading.line_stats = false
    )

    // Line stats
    this.loading.times_stats = true
    this._report.times_stats().subscribe(
        data => {
            this.data.times_stats = data
        },
        err => this.callback_err("times_stats"),
        () => this.loading.times_stats = false
    )

    // Users stats
    this.loading.users_stats = true
    this._report.users_stats().subscribe(
        (data:any) => {

            let back_obj = data.find(e => e.status == USER_STATUS.BACK_5)
            let break_obj = data.find(e => e.status == USER_STATUS.BREAK)
            this.data.users_stats = {
                online: data.find(e => e.status == USER_STATUS.ONLINE).total,
                back_5: back_obj ? back_obj.total : 0,
                break: break_obj ? break_obj.total : 0
            }
        },
        err => this.callback_err("users_stats"),
        () => this.loading.users_stats = false
    )

    // Tickets stats
    this.loading.tickets_stats = true
    this._report.tickets_stats().subscribe(
        (data:any) => {

            let success = data.find(e => e.status == TICKET_STATUS.SUCCESS)
            let rejected = data.find(e => e.status == TICKET_STATUS.REJECTED)
            let once = data.find(e => e.status == TICKET_STATUS.TEMP_REJECTED)
            this.data.tickets_stats = {
                success:    success ? success.total : 0,
                rejected:   rejected ? rejected.total : 0,
                once:       once ? once.total : 0
            }
        },
        err => this.callback_err("tickets_stats"),
        () => this.loading.tickets_stats = false
    )
  }

  callback_err(card){
    this._toast.error(this._trans.instant("Error trying to get") + " " + card, this._trans.instant('Error'))
  }

/*
  lineChartData: ChartDataSets[] = [
    { data: [563, 845, 1345, 1425, 980, 605, 325, 102], label: 'Report' },
  ];

  lineChartLabels: Label[] = ['10am', '12pm', '02pm', '04pm', '06pm', '08pm', '10pm', '12am'];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#01A66E',
      backgroundColor: 'rgba(1,166,111,0.2)',
    },
  ];

  lineChartLegend = false;
  lineChartPlugins = [];
  lineChartType = 'line';
*/

}
