import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { timer } from 'rxjs';
import { environment } from '@env/environment'
import { AuthenticationService } from 'src/app/_services';
import { TranslateService } from '@ngx-translate/core';
import { BroadcastService } from '@azure/msal-angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: []
})
export class LoginComponent implements OnInit {
  
  env = environment;
  form_login: FormGroup
  loading = false;
  loading_office = false;
  isCollapsed = false;
  submitted = false;
  return_url: string;
  error = '';

  constructor(
    private _formbuilder: FormBuilder,
    private _auth: AuthenticationService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _broadcastService: BroadcastService,
    private _trans: TranslateService){

        if (this._auth.currentUserValue) {
            this._router.navigate(['/']);
        }

    }

  ngOnInit() {

    var tokenRequest = {
        scopes: ["user.read"]
    };

    this._auth.msalService.handleRedirectCallback((authError, response) => {
        this.loading_office = true;

        this._broadcastService.subscribe("msal:loginSuccess", payload => {

            this._auth.msalService.acquireTokenSilent(tokenRequest)
            .then(result => {
                if (result != undefined) {
                    this.login_acquired_token(result['accessToken']);
                }
            }).catch(error => {
                console.log('Cannot Acquire Access Token');
                var loginRequest = {
                    scopes: ["user.read"]
                };
                this._auth.msalService.loginRedirect(loginRequest);
            });
        });
    });




    this.form_login = this._formbuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })

    this.return_url = this._route.snapshot.queryParams['return_url'] || '/';
  }

  get f() { return this.form_login.controls; }

  submit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form_login.invalid)
      return;

    this.loading = true;
    this._auth.login_or_token(this.f.username.value.toLowerCase(), this.f.password.value, 'login')
      .pipe(first())
      .subscribe(
        data => {
          this._router.navigate([this.return_url]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }


  /*
  Refactor  Office 365: Sends the token to the and username to AuthenticationService,
  and the endpoint to use, token in this case.
  */

  login_acquired_token(token) {
    var user = this._auth.get_microsoft_account()['userName'];
    this._auth.login_or_token(user, token, 'token')
      .pipe(first())
      .subscribe(
        data => {
          this._router.navigate([this.return_url]);
        },
        error => {
          this.error = error;
        });
  }

  /*
  Refactor  Office 365:
  Cathes the click evnet from the button to login with office 365. Request the acces token.
  If the user is not logged popsUp a request.
  */

  login_microsoft_click() {

    this.loading_office = true;

    var loginRequest = {
        scopes: ["user.read"]
    };

    this._auth.msalService.loginRedirect(loginRequest);

    }

}
