import { Component, OnInit, ViewChild, ViewContainerRef  } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TicketsService, ToastService, ExternalService, AuthenticationService, PushNotificationsService } from '@app/_services';
import { WebsocketModel, TICKET_COMMANDS, TICKET_STATUS, USER_STATUS, USER_LEVELS } from '@app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StatusService } from '@app/_services/status.service';
import { environment } from '@env/environment';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styles: []
})
export class InboxComponent implements OnInit {
  @ViewChild('tabs', { static: false })
  public tabs: NgbTabset;

  inbox = []
  working = []
  aux = {
    ticket_status: TICKET_STATUS,
    message_prefixes: [],
    users_online: [],
    url_media: environment.URL_BACK_MEDIA,
    url_assets: environment.URL_BASE_ASSETS,
    sending: [],
    current_tab: null,
    inbox_tab_id: 'inbox',
    loading_inbox: false
  }

  cpf_info = {
    blank: {
      cpf:null,
      birthdate:null,
      cpf_status:null,
      cpf_updated_at:null
    },
    no_info: {
      cpf: 'No info',
      birthdate: 'No info',
      cpf_status: null,
      cpf_updated_at: 'No info',
    }
  }

  constructor(
    private _ws_tickets: TicketsService,
    private _toast: ToastService,
    private _status: StatusService,
    private _external: ExternalService,
    private _auth: AuthenticationService,
    private _builder: FormBuilder,
    private _router: Router,
    private _sanitizer: DomSanitizer,
    private _pushNotifications: PushNotificationsService,
    private _trans: TranslateService
    ) { }

  ngOnInit() {
    // Request notification permission
    this._pushNotifications.requestPermission();

    this._ws_tickets.hub.subscribe(
      res => {
        res = <WebsocketModel>res.message;

        if (res.command == TICKET_COMMANDS.GET_INBOX)
          this.callback_get_inbox(res.data)
        else if (res.command == TICKET_COMMANDS.NEW_TICKET)
          this.callback_new_ticket(res.data)
        else if (res.command == TICKET_COMMANDS.UPDATE_TICKET)
          this.callback_update_ticket(res.data)
        else if (res.command == TICKET_COMMANDS.WORK_TICKET)
          this.callback_work_ticket(res.data)
        else if (res.command == TICKET_COMMANDS.SAVE_TICKET_SUCCESS)
          this.callback_save_ticket_success(res.data)
        else if (res.command == TICKET_COMMANDS.GET_USERS_ONLINE)
          this.callback_get_users_online(res.data)
        else if (res.command == TICKET_COMMANDS.GET_TICKET)
          this.callback_work_ticket(res.data, true)
        else if (res.command == TICKET_COMMANDS.FORCE_LOGOUT)
          this.callback_force_logout()
        else if (res.command == TICKET_COMMANDS.GET_TICKET_BY_STATUS)
          this.callback_get_ticket_by_status(res.data)
        else if (res.command == TICKET_COMMANDS.TOAST)
          this.callback_toast(res.data)
        else
          console.log("Command not found: ", res)
      },
      err => {
        this.callback_error();
      },
      () => {
        this.callback_error();
      }
    );

    this._ws_tickets.is_open().subscribe(open => {
      this.aux.loading_inbox = true
      this._ws_tickets.send(TICKET_COMMANDS.GET_INBOX, null)
    })

    this._status.get_status().subscribe(
      data => {
        if (data == USER_STATUS.ONLINE)
          this._ws_tickets.send(TICKET_COMMANDS.SERVE, null)
      }
    )
  }

  /* CALLBACKS */
  callback_error() {
    this._toast.error("Error al intentar conectar con el servidor. Recargando...")
    setTimeout(() => location.reload(), 500)
  }

  callback_new_ticket(data) {
    this.inbox.push(data)
  }

  callback_work_ticket(data, from_get_ticket = false) {
    console.log('callback_work_ticket', data)

    if (from_get_ticket)
      this.aux.sending[data.id] = false

    if (this.working.findIndex(x => x.id == data.id) >= 0)
      return

    this.add_extras(data)

    this.working.push(data)

    console.log("Current 2:", this.tabs.activeId)
    if (this.tabs.activeId == this.aux.inbox_tab_id || from_get_ticket)
        this.select_tab(data.id)

    this.notify("Hey, you have been assigned a new ticket")
  }

  callback_get_inbox(data) {
    this.inbox = data.inbox
    this.aux.loading_inbox = false

    this.working = data.working
    this.aux.message_prefixes = data.messages
    console.log(this.working)
    this.working.forEach((item) => {
      this.add_extras(item)
    })
  }


  callback_update_ticket(data) {
    console.log('callback_update_ticket', data)
    var index = this.inbox.findIndex(x => x.id == data.id);
    if (data.status > TICKET_STATUS.WORKING)
      this.inbox.splice(index, 1)
    else if (data.status == TICKET_STATUS.WORKING)
      this.inbox[index] = data
  }

  callback_save_ticket_success(data) {
    console.log('callback_save_ticket_success', data)
    var index = this.working.findIndex(x => x.id == data.id);
    this.working.splice(index, 1)

    console.log('Serve new ticket')
    this._ws_tickets.send(TICKET_COMMANDS.SERVE, null)

    if (this.working.length > 0)
        this.select_tab(this.working[0].id)
  }

  callback_get_users_online(data) {
    console.log('callback_get_users_online', data)
    this.aux.users_online = data;
  }

  callback_force_logout() {
    console.log('callback_force_logout')
    this._auth.logout();
    this._router.navigate(['/login']);
  }

  callback_get_ticket_by_status(data){
    console.log('callback_get_ticket_by_status',data)

    var index = this.working.findIndex(x => x.id == data.ticket);

    let success = data.data.find(e => e.status == TICKET_STATUS.SUCCESS)
    let rejected = data.data.find(e => e.status == TICKET_STATUS.REJECTED)
    let once = data.data.find(e => e.status == TICKET_STATUS.TEMP_REJECTED)
    this.working[index].by_status_info = {
        success:    success ? success.total : 0,
        rejected:   rejected ? rejected.total : 0,
        once:       once ? once.total : 0
    }
  }

  callback_toast(data){
    if (data.level == 'E')
        this._toast.error(data.message)
    else if (data.level == 'S')
        this._toast.success(data.message)
  }


  /* METHODS */

  add_extras(item) {
    // Set read only
    item.read_only = this._auth.currentUserValue.id != item.last_user.id
    if (!item.read_only)
      item.read_only = item.status != TICKET_STATUS.WORKING

    // Set functions
    item.is_status = function (status) {
      return item.status == status
    }

    // Create form
    let default_message_prefix = this.aux.message_prefixes[0]
    item.form = this._builder.group({
      message: [default_message_prefix.id, Validators.required]
    })

    // Set booleans
    item.submitted = false
    item.sending = {}
    item.sending.some = false
    item.sending[this.aux.ticket_status.SUCCESS] = false
    item.sending[this.aux.ticket_status.REJECTED] = false
    item.sending[this.aux.ticket_status.TEMP_REJECTED] = false

    // Get CPF info
    item.cpf_info = this.cpf_info.blank
    this._external.get_cpfinfo(item.account_id).subscribe(
      (data: any) => {
        if (data.success) {
          item.cpf_info = {
            cpf: data.result[2],
            birthdate: data.result[3],
            cpf_status: data.result[4],
            cpf_updated_at: data.result[5],
          }
        } else
          item.cpf_info = this.cpf_info.no_info
      },
      err => {
        console.log("get_cpfinfo => ", err)
        item.cpf_info = this.cpf_info.no_info
      }
    )

    // Prepare some fields
    item.platform = item.platform.toLowerCase()
    item.os = item.os.toLowerCase().split(' ')[0]
    item.browser = item.browser.toLowerCase().split(' ')[0]

    // Form to transfer
    item.form_tranfer = this._builder.group({
      user: ['', Validators.required]
    })

    // Allowed to tranfer
    item.allowed_to_transfer = !item.read_only || this._auth.currentUserValue.level >= USER_LEVELS.SUPERVISOR

    // Safe file url
    item.safe_url =  this._sanitizer.bypassSecurityTrustResourceUrl(environment.URL_BACK_MEDIA + item.file.file);

    // Get customer's tickets by status
    item.by_status_info = {success:null}
    let data_by_status = {
        account_id: item.account_id,
        ticket: item.id
    }
    this._ws_tickets.send(TICKET_COMMANDS.GET_TICKET_BY_STATUS, data_by_status)
  }


  save(item, status) {
    item.submitted = true

    if (item.form.invalid)
      return;

    item.sending[status] = item.sending.some = true
    let data = {
      id: item.id,
      status: status,
      message: item.form.value.message
    }
    this._ws_tickets.send(TICKET_COMMANDS.SAVE_TICKET, data)
  }

  is_pdf(filename: string) {
    return filename.endsWith('.pdf');
  }

  load_online($event) {
    this._ws_tickets.send(TICKET_COMMANDS.GET_USERS_ONLINE, null)
  }

  tranfer_ticket(item) {
    if (item.form_tranfer.invalid)
      return

    let data = {
      user: item.form_tranfer.value['user'],
      ticket: item.id,
    }

    this._ws_tickets.send(TICKET_COMMANDS.TRANSFER_TICKET, data)
  }

  get_ticket(ticket_id) {
    this.aux.sending[ticket_id] = true
    this._ws_tickets.send(TICKET_COMMANDS.GET_TICKET, { ticket: ticket_id })
  }

  select_tab(id, delay = true) {
    if (delay) {
      let self = this
      setTimeout(() => self.tabs.select(id), 100)
    } else
      this.tabs.select(id);
  }

  close_tab(id){
    var index = this.working.findIndex(x => x.id == id);
    this.working.splice(index, 1)
  }

  copy_to_clipboard(text){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

    dataURItoBlob(dataURI) {
       const byteString = window.atob(dataURI);
       const arrayBuffer = new ArrayBuffer(byteString.length);
       const int8Array = new Uint8Array(arrayBuffer);
       for (let i = 0; i < byteString.length; i++) {
         int8Array[i] = byteString.charCodeAt(i);
       }
       const blob = new Blob([int8Array], { type: 'application/pdf' });
       return blob;
    }

    notify(key_message){
        let options = {
          body: this._trans.instant(key_message),
          icon: this.aux.url_assets + "/img/logo_sm.png"
        }
         this._pushNotifications.create('Attis', options).subscribe(
            res => console.log(res),
            err => console.log(err)
        );
      }


}

