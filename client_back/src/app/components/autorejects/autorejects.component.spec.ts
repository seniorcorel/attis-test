import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorejectsComponent } from './autorejects.component';

describe('AutorejectsComponent', () => {
  let component: AutorejectsComponent;
  let fixture: ComponentFixture<AutorejectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorejectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorejectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
