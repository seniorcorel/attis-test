/* Component CRUD to Autorejects. */

import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ToastService, AutorejectsService, MessageprefixesService } from '@app/_services';
import { TranslateService } from '@ngx-translate/core';
import { Page } from '@app/_models';
import { DatePipe } from '@angular/common';
import { environment } from '@env/environment'

@Component({
  selector: 'app-autorejects',
  templateUrl: './autorejects.component.html',
  styles: []
})
export class AutorejectsComponent implements OnInit {
  
  env = environment
  list = []
  id: Number
  form: FormGroup
  aux = {
    page_size: 10,
    page: 1,
    page_total: 10,
    modal_reference: null,
    sending: false,
    submitted: false,
    error_form: null,
    messages: []
  }

  account_id = "";

  constructor(
    private _api: AutorejectsService,
    private _messageprefixes: MessageprefixesService,
    private _builder: FormBuilder,
    private _toast: ToastService,
    private _trans: TranslateService,
    private _config: NgbModalConfig,
    private _modalService: NgbModal,
    private _date_pipe: DatePipe) {
    this._config.backdrop = 'static'
    this._config.keyboard = false
  }

  // Convenience getter for easy access to form fields.
  get f() { return this.form.controls; }

  ngOnInit() {
    this.get_data()

    this._messageprefixes.get().subscribe(
      (data:any) => this.aux.messages = data
    )
  }

  open_modal(content) {
    /* Open modal and save reference. */
    this.aux.modal_reference = this._modalService.open(content, { centered: true });
  }

  get_data() {
    /* Get data (paginated) from api. */

    if (!this.account_id) {

        this._api.get_paginated({page:this.aux.page}).subscribe(
          (res: Page<any>) => {
            this.list = res.results
            this.aux.page_total = res.count
          },
          err => {
            console.log(err)
          }
        )
    } else {

        this._api.get_paginated({page:this.aux.page, account_id:this.account_id}).subscribe(
          (res: Page<any>) => {
            this.list = res.results
            this.aux.page_total = res.count
          },
          err => {
            console.log(err)
          }
        )

    }


  }

  get_data_by_page($event){
    /* Called from pagination component. */
    this.get_data();
  }

  open_form(content, item = null) {
    /*  Create form and open modal.
        If item is not null then form will be to edit. */

    this.id = (item != null) ? item.id : null

    this.form = this._builder.group({
      account_id: [(item != null) ? item.account_id : '', [Validators.required]],
      expire: [(item != null) ? this._date_pipe.transform(item.expire, 'yyyy-MM-dd') : ''],
      message: [(item != null) ? item.message.id : '', [Validators.required]],
    })
    this.open_modal(content)
  }

  save() {
    /*  Send form value to api.
        If id is null will create, else will edit object. */
    this.aux.submitted = true

    if (this.form.invalid)
      return;

    if (this.form.value.expire == '')
      delete this.form.value.expire
    else
      this.form.value.expire = new Date(this.form.value.expire)
    

    this.aux.sending = true
    if (this.id != null)
      this._api.put(this.id, this.form.value).subscribe(
        data => this.callback_success(data),
        err => this.callback_error(err)
      )
    else
      this._api.post(this.form.value).subscribe(
        data => this.callback_success(data),
        err => this.callback_error(err)
      )      
  }

  filterAccountID(){

    if (!this.account_id) {
        this._api.get_paginated({page:1}).subscribe(
          (res: Page<any>) => {
            this.list = res.results
            this.aux.page_total = res.count
          },
          err => {
            console.log(err)
          }
        )
    } else {
        this._api.get_paginated({page:1, account_id:this.account_id}).subscribe(
          (res: Page<any>) => {
            this.list = res.results
            this.aux.page_total = res.count
          },
          err => {
            console.log(err)
          }
        )
    }
  }


  callback_success(data) {
    /* When object saved successfully. */    
    this.aux.sending = this.aux.submitted = false
    this.aux.modal_reference.close()
    this._toast.success(this._trans.instant('Saved successfully'), this._trans.instant('Success'))
    this.get_data()
  }

  callback_error(error) {
    /* When save return error. */
    this.aux.error_form = error
    this.aux.sending = false
  }

  delete(id) {
    /* Send delete request to api. */
    this._api.delete(id).subscribe(
      date => {
        this._toast.success(this._trans.instant('Deleted successfully'), this._trans.instant('Success'))
        this.get_data()
      },
      err => {
        this._toast.error(this._trans.instant(err), this._trans.instant('Error'))
      }
    )
  }

}
