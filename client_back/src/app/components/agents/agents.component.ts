import { Component, OnInit } from '@angular/core';
import { UsersService, ToastService } from '@app/_services';
import { USER_STATUS } from '@app/_models'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styles: []
})
export class AgentsComponent implements OnInit {

  list = []
  status = USER_STATUS
  sending_logout = []
  sending_autoassign = []

  constructor(
    private _user: UsersService,
    private _toast: ToastService,
    private _trans: TranslateService,
  ) { }

  ngOnInit() {
    this.get_data();
  }

  get_data(){
    this._user.users_now().subscribe(
      (data:any) => this.list = data
    )
  }

  force_logout(user_id){
    this.sending_logout[user_id] = true
    this._user.force_logout(user_id).subscribe(
      data => {
        this.sending_logout[user_id] = false
        this._toast.success(this._trans.instant('Logout successfully'), this._trans.instant('Success'))
        this.get_data();
      }
    )
  }

  update_autoassign(user_id, value){
    this.sending_autoassign[user_id] = true
    this._user.put(user_id, {'has_autoassign':value}).subscribe(
      data => {
        this.sending_autoassign[user_id] = false
        this._toast.success(this._trans.instant('Saved successfully'), this._trans.instant('Success'))
        this.get_data();
      }
    )
  }


}
