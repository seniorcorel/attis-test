import { Component, OnInit } from '@angular/core';
import { AuthenticationService, ToastService } from 'src/app/_services';
import { Router } from '@angular/router';
import { environment } from '@env/environment'
import { StatusService } from '@app/_services/status.service';
import { USER_STATUS, USER_LEVELS } from '@app/_models'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styles: []
})
export class NavComponent implements OnInit {
  
  env = environment
  user_status = USER_STATUS
  user_level = USER_LEVELS
  currentUser = {username:'', level:0, has_autoassign:false};
  current_status = null
  status_sending = false

  constructor(
    private _auth: AuthenticationService,
    private _router: Router,
     private _status: StatusService,
     private _toast: ToastService,
     private _trans: TranslateService) {
    this._auth.currentUser.subscribe(x => {
      if (x != null)
        this.currentUser = x
    });

    this._status.get_status().subscribe(
      data => {
        this.current_status = data
        this.status_sending = false
      },
      err => this.status_sending = false
    )
  }

  ngOnInit() {
  }

  logout() {
    this._status.logout().subscribe(
      data => {
        this._auth.logout();
        //this._router.navigate(['/login']);
      },
      err => this._toast.error(this._trans.instant(err), this._trans.instant('Error changing status'))
    );

    this._router.navigate(['/login']);
  }

  change_status(status){
    this.status_sending = true;
    this.current_status = status
    this._status.set_status(this.current_status);
  }

  enabled(level){
    return this.currentUser.level >= level
  }

}