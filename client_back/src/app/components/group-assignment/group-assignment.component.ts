import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AssignmentsService, UsersService, GroupsService, ToastService } from '@app/_services';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '@env/environment'

@Component({
  selector: 'app-group-assignment',
  templateUrl: './group-assignment.component.html',
  styles: []
})
export class GroupAssignmentComponent implements OnInit {
  
  env = environment
  list_users = []
  list_groups = []
  form_byuser: FormGroup
  aux = {
    sending: false,
    submitted: false,
    error_form_byuser: null,
  }

  constructor(
    private _api: AssignmentsService,
    private _api_users: UsersService,
    private _api_groups: GroupsService,
    private _toast: ToastService,
    private _trans: TranslateService,
    private _builder: FormBuilder,
    private _cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this._api_users.get().subscribe(
      (data: any) => this.list_users = data,
      err => this._toast.error(this._trans.instant('Error geting users'))
    )

    this._api_groups.get().subscribe(
      (data: any) => this.list_groups = data,
      err => this._toast.error(this._trans.instant('Error geting groups'))
    )

    this.form_byuser = this._builder.group({
      user: ['', Validators.required],
      groups: ['', Validators.required]
    })

    this._cd.detectChanges();
  }

  submit(form) {
    this.aux.submitted = true

    if (form.invalid)
      return;

    this.aux.sending = true
    this._api.byuser(form.value).subscribe(
      data => this.callback_success(data),
      err => this.callback_error(err)
    )
  }

  callback_success(data) {
    /* When object saved successfully. */
    this.aux.sending = this.aux.submitted = false
    this._toast.success(this._trans.instant('Saved successfully'), this._trans.instant('Success'))
    this.form_byuser.reset()
  }

  callback_error(err) {
    /* When save return error. */
    this.aux.sending = this.aux.submitted = false
    this._toast.error(this._trans.instant(err), this._trans.instant('Error'))
  }

  select_mygroups($event) {
    if ($event == null)
      this.form_byuser.value.groups = null
    else
      this._api.get_mygroups($event.id).subscribe(
        (data: any) => this.form_byuser.controls.groups.setValue(data.map(item => item.id)),
        err => this._toast.error(this._trans.instant('Error geting groups') + ": " + err, this._trans.instant('Error'))
      )
  }

}
