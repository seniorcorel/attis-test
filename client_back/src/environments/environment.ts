// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  URL_BASE_ASSETS: 'http://localhost:4200/assets/',
  URL_API: 'http://localhost:8000/api/',
  URL_WS: 'ws://localhost:8000/ws/tickets/',
  URL_BACK_MEDIA: 'http://localhost:8000',
  URL_SYNA_API: 'http://atlas.gomifer.local/syna/api/Microservices/GetCpfStatus?accountid=',
  URL_DASHBOARD: 'http://metrics.attis.prod.gomifer.com/',
  URL_AZURE_REDIRECT: 'http://localhost:4200/',
  URL_AZURE_POSTLOGOUT: 'http://localhost:4200/login',
  AZURE_CLIENT_ID: 'fb998aee-f53b-4e13-98a9-5c0918cedde0',
  LOGIN_DOMAIN: 'gvcgroup',
  AZURE_TENANTID: 'https://login.microsoft.com/aa36113a-6c52-4d65-8088-8232af04e2f8'

  }

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
