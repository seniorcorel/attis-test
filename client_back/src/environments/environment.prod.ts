export const environment = {
  production: true,
  URL_BASE_ASSETS: window["env"]["URL_BASE_ASSETS"],
  URL_API: window["env"]["URL_API"],
  URL_WS: window["env"]["URL_WS"],
  URL_BACK_MEDIA: window["env"]["URL_BACK_MEDIA"],
  URL_SYNA_API: window["env"]["URL_SYNA_API"],
  URL_DASHBOARD: window["env"]["URL_DASHBOARD"],
  URL_AZURE_REDIRECT: window["env"]["URL_AZURE_REDIRECT"],
  URL_AZURE_POSTLOGOUT: window["env"]["URL_AZURE_POSTLOGOUT"],
  AZURE_CLIENT_ID: window["env"]["AZURE_CLIENT_ID"],
  LOGIN_DOMAIN: window["env"]["LOGIN_DOMAIN"],
  AZURE_TENANTID: window["env"]["AZURE_TENANTID"]
};
