(function(window) {
  window["env"] = window["env"] || {};

  window["env"]["URL_BASE_ASSETS"] = "${URL_BASE_ASSETS}";
  window["env"]["URL_API"] = "${URL_API}";
  window["env"]["URL_WS"] = "${URL_WS}";
  window["env"]["URL_BACK_MEDIA"] = "${URL_BACK_MEDIA}";
  window["env"]["URL_SYNA_API"] = "${URL_SYNA_API}";
  window["env"]["URL_DASHBOARD"] = "${URL_DASHBOARD}";
  window["env"]["URL_AZURE_REDIRECT"] = "${URL_AZURE_REDIRECT}";
  window["env"]["URL_AZURE_POSTLOGOUT"] = "${URL_AZURE_POSTLOGOUT}";
  window["env"]["AZURE_CLIENT_ID"] = "${AZURE_CLIENT_ID}";
  window["env"]["LOGIN_DOMAIN"] = "${LOGIN_DOMAIN}";
  window["env"]["AZURE_TENANTID"] = "${AZURE_TENANTID}";

})(this);