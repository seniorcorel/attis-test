\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Purpose}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}The Attis project}{3}{subsection.1.2}%
\contentsline {section}{\numberline {2}Infrastructure}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Environments}{4}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Production}{4}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}QAS}{4}{subsubsection.2.1.2}%
\contentsline {subsubsection}{\numberline {2.1.3}Local}{4}{subsubsection.2.1.3}%
\contentsline {subsection}{\numberline {2.2}Data Bases}{4}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Production}{4}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}QAS}{4}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Local}{4}{subsubsection.2.2.3}%
\contentsline {subsection}{\numberline {2.3}Repository}{4}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Gitflow}{4}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}CI/CD}{5}{subsubsection.2.3.2}%
\contentsline {section}{\numberline {3}Project Directories}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}.envs}{6}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}client back}{6}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}client front}{6}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}compose}{6}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}deploy}{6}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}docs}{6}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}server}{7}{subsection.3.7}%
\contentsline {subsubsection}{\numberline {3.7.1}Attis V2}{7}{subsubsection.3.7.1}%
\contentsline {section}{\numberline {4}Change Log}{8}{section.4}%
\contentsline {section}{Appendices}{9}{section*.4}%
\contentsline {section}{\numberline {A}CI/CD YAML}{9}{appendix.1.A}%
\contentsline {section}{\numberline {B}Local Build YAML}{12}{appendix.1.B}%
\contentsline {section}{\numberline {C}Production Build YAML}{15}{appendix.1.C}%
