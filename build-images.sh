#!/bin/sh

dif_django=$(git diff HEAD~1 ./server/attis_v2)
dif_postgres=$(git diff HEAD~1 ./compose/production/postgres)
dif_ngback=$(git diff HEAD~1 ./client_back)
dif_ngfront=$(git diff HEAD~1 ./client_front)


if [ ${#dif_django} -ge 5 ]
then
	docker-compose -f ./"${1}".yml build django celeryworker celerybeat flower

	docker tag registry.gitlab.com/it-barcelona/attis_v2/"${1}"_django registry.gitlab.com/it-barcelona/attis_v2/"${1}"_django:"${2}"
	docker tag registry.gitlab.com/it-barcelona/attis_v2/"${1}"_celeryworker registry.gitlab.com/it-barcelona/attis_v2/"${1}"_celeryworker:"${2}"
	docker tag registry.gitlab.com/it-barcelona/attis_v2/"${1}"_celerybeat registry.gitlab.com/it-barcelona/attis_v2/"${1}"_celerybeat:"${2}"
	docker tag registry.gitlab.com/it-barcelona/attis_v2/"${1}"_flower registry.gitlab.com/it-barcelona/attis_v2/"${1}"_flower:"${2}"

	docker push registry.gitlab.com/it-barcelona/attis_v2/"${1}"_django:"${2}"
	docker push registry.gitlab.com/it-barcelona/attis_v2/"${1}"_celeryworker:"${2}"
	docker push registry.gitlab.com/it-barcelona/attis_v2/"${1}"_celerybeat:"${2}"
	docker push registry.gitlab.com/it-barcelona/attis_v2/"${1}"_flower:"${2}"
fi

if [ ${#dif_postgres} -ge 5 ] && [ "${1}" = "local" ]
then
	docker-compose -f ./local.yml build postgres
	docker tag registry.gitlab.com/it-barcelona/attis_v2/production_postgres registry.gitlab.com/it-barcelona/attis_v2/production_postgres:"${2}"
	docker push registry.gitlab.com/it-barcelona/attis_v2/production_postgres:"${2}"
fi

if [ ${#dif_ngback} -ge 5 ]
then
	docker-compose -f ./"${1}".yml build ngback
	docker tag registry.gitlab.com/it-barcelona/attis_v2/"${1}"_ngback registry.gitlab.com/it-barcelona/attis_v2/"${1}"_ngback:"${2}"
	docker push registry.gitlab.com/it-barcelona/attis_v2/"${1}"_ngback:"${2}"
fi

if [ ${#dif_ngfront} -ge 5 ]
then
	docker-compose -f ./"${1}".yml build ngfront
	docker tag registry.gitlab.com/it-barcelona/attis_v2/"${1}"_ngfront registry.gitlab.com/it-barcelona/attis_v2/"${1}"_ngfront:"${2}"
	docker push registry.gitlab.com/it-barcelona/attis_v2/"${1}"_ngfront:"${2}"
fi
