/* ---------------- IS VIP ---------------- */
CREATE OR REPLACE FUNCTION is_vip(g integer) 
RETURNS BOOLEAN AS $func$
DECLARE pivote integer := 5;
BEGIN		
	IF g <= pivote THEN
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END; 
$func$ LANGUAGE plpgsql;

/* UNIT TEST */
select is_vip(1); -- TRUE
select is_vip(6); -- FALSE


/* ---------------- GET COUNT TICKET IN DATE ---------------- */
CREATE OR REPLACE FUNCTION get_count_ticket_in_date(_account_id bigint, _start timestamp with time zone, _status int = 0) 
RETURNS integer AS $func$
BEGIN		
	IF _status = 0 THEN
		RETURN (Select count(id) from core_ticket t where t.account_id=_account_id and arrived>=_start and arrived<= (_start + interval '1' day));
	ELSE
		RETURN (Select count(id) from core_ticket t where t.account_id=_account_id and arrived>=_start and arrived<= (_start + interval '1' day) and t.status=_status);
	END IF;
END; 
$func$ LANGUAGE plpgsql;

/* UNIT TEST */
select get_count_ticket_in_date(111, '20200319'); -- 1



/* ---------------- INSISTENCE INDEX ---------------- */
CREATE OR REPLACE FUNCTION insistence_index(_files int, _submits int) 
RETURNS decimal(4,1) AS $func$
BEGIN		
	RETURN ROUND(100 - ((_files/_submits::decimal) *100),1);
END; 
$func$ LANGUAGE plpgsql;

/* UNIT TEST */
select insistence_index(10, 10); -- 0
select insistence_index(1, 10); -- 90


/* ---------------- COUNT WEEKDAY IN RANGE ---------------- */
CREATE OR REPLACE FUNCTION count_dow_in_range(_start timestamp with time zone, _end timestamp with time zone, _dow double precision) 
RETURNS integer AS $func$
BEGIN		
	RETURN (select count(*) from generate_series(_start, _end, interval '1d') d where extract(isodow from d)=_dow);
END; 
$func$ LANGUAGE plpgsql;

/* UNIT TEST */
select count_dow_in_range('20200301', '20200331', 1); -- Monday: 5
select count_dow_in_range('20200301', '20200331', 5); -- Friday: 4